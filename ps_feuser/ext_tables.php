<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}
$tempColumns = array(
	'tx_psfeuser_beacons' => array(		
		'exclude' => 0,		
		'label' => 'LLL:EXT:ps_feuser/locallang_db.xml:fe_users.tx_psfeuser_beacons',		
		'config' => array(
			'type' => 'input',	
			'size' => '50',
			'readOnly' => 1,
		)
	),
);


t3lib_div::loadTCA('fe_users');
t3lib_extMgm::addTCAcolumns('fe_users',$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('fe_users','tx_psfeuser_beacons');
?>