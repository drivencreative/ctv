<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "ps_feuser".
 *
 * Auto generated 22-12-2015 13:52
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Peak Sourcing',
	'description' => '',
	'category' => 'fe',
	'author' => 'Miladin Bojic',
	'author_email' => 'miladin.bojic@gmail.com',
	'state' => 'alpha',
	'uploadfolder' => 0,
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => 
	array (
		'depends' => 
		array (
			'cms' => '',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
	'_md5_values_when_last_written' => 'a:8:{s:9:"ChangeLog";s:4:"d9b7";s:12:"ext_icon.gif";s:4:"1bdc";s:14:"ext_tables.php";s:4:"3596";s:14:"ext_tables.sql";s:4:"833d";s:16:"locallang_db.xml";s:4:"a047";s:10:"README.txt";s:4:"ee2d";s:19:"doc/wizard_form.dat";s:4:"8d51";s:20:"doc/wizard_form.html";s:4:"1653";}',
);

