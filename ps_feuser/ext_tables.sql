#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users (
	tx_psfeuser_push_notifications tinyint(3) DEFAULT '0' NOT NULL,
	tx_psfeuser_beacons text,
	tx_psfeuser_favorites text,
	tx_psfeuser_api_token text,
	tx_psfeuser_gewinnspiel_submitted tinyint(3) DEFAULT '0' NOT NULL,
	tx_psfeuser_ticket_file_name text,
	tx_psfeuser_rayseven_password text,
);