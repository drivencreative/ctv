<?php
namespace PS\CsvImport\Utility;

/**
 * Created by PhpStorm.
 * User: Dragan
 * Date: 2/27/2017
 * Time: 1:17 PM
 */
use TYPO3\CMS\Backend\Form\Wizard\SuggestWizardDefaultReceiver;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\Utility\IconUtility;


class PersImport
{
    /**
     * csv import for testing
     *
     * @param string $csvPath
     * @return array
     * @static
     */
    public static function importCsvData($csvPath)
    {
        $persData = array();
        $handle = fopen($csvPath, "r");
        if ($handle) {
            $titles = fgetcsv($handle, 1000, ",");
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $data = array_map("utf8_encode", $data); //added
                $datacomplete = array();
                foreach($titles as $index=>$value) {
                    $datacomplete[$value] = $data[$index];
                }
                $persData[] = $datacomplete;
            }
            fclose($handle);
        }
        return $persData;
    }
}