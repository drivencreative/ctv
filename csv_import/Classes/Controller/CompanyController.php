<?php
namespace PS\CsvImport\Controller;


    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * CompanyController
 */
class CompanyController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * @var
     */
    protected $currentConfig;

    /**
     * companyRepository
     *
     * @var \PS\PsTables\Domain\Repository\CompanyRepository
     * @inject
     */
    protected $companyRepository = null;

    /**
     * backendRepository
     *
     * @var \PS\CsvImport\Domain\Repository\BackendRepository
     * @inject
     */
    protected $backendRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {

        if ($this->request->hasArgument('file')) {
            $this->importAction($this->request->getArgument('file'), $this->request->getArgument('pid'), $this->request->getArgument('path'));
        }
        $companies = $this->companyRepository->findAll();
        $this->view->assign('companies', $companies);
    }

    /**
     * action show
     *
     * @param \PS\CsvImport\Domain\Model\Company $company
     * @return void
     */
    public function showAction(\PS\CsvImport\Domain\Model\Company $company)
    {
        $this->view->assign('company', $company);
    }

    /**
     * action import
     *
     * @return void
     */
    public function importAction($file = [], $pid= 0, $path= '')
    {
        $file = $file ? $file['tmp_name'] : '';
        $pid = $pid ? $pid['pid'] : 34;
        $path = empty($path) ? $path['path'] : 'user_upload/tx_csvimport/';
        $data = $this->backendRepository->createCsvArray($file);
        $categoryRepository = $this->objectManager->get('\Ps\PsTables\Domain\Repository\CompanyCategoryRepository');
        foreach ($data as $company) {
            /* Company Model */
            $newCompany = $this->objectManager->get('Ps\PsTables\Domain\Model\Company');
            
           

            $newCompany->setTitle($company['title']);
            $newCompany->setLocation($company['location']);

            /* Image file */
            if (isset($company['image'])) {
                /* Company Model */
                $newImage = $this->objectManager->get('Ps\PsTables\Domain\Model\Image');
                /* File Reference Model */
                $fileRef = $this->objectManager->get('\Ps\PsTables\Domain\Model\FileReference');
                
                $storage = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Resource\StorageRepository')->findByUid(1);
                $fileRef->setOriginalResource($storage->getFile('/' . $path . $company['image']));
                $newImage->setImage($fileRef);
                $newImage->setPid($pid);

                $newCompany->setImage($newImage);
            }

            /* Companies */
            $newCompanyCategories = $categoryRepository->findByUids($company['category']);
            foreach ($newCompanyCategories as $newCompanyCategory) {
                $newCompany->addCategory($newCompanyCategory);
            }

            /* Add new Company record*/
            $this->companyRepository->add($newCompany);
        }
        $this->persistenceManager->persistAll();
        $this->view->assign('newCompanies', $data);
    }
}