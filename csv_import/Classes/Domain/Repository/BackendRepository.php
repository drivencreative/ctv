<?php
/**
 * Created by PhpStorm.
 * User: Dragan
 * Date: 2/27/2017
 * Time: 1:26 PM
 */

namespace PS\CsvImport\Domain\Repository;
use PS\CsvImport\Utility\PersImport;


class BackendRepository extends \TYPO3\CMS\Extbase\Persistence\Repository{
    public function createCsvArray($csvPath = ''){
        if($csvPath != '') {
            return PersImport::importCsvData($csvPath);
        }
    }
}