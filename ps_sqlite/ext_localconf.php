<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Peaksourcing.' . $_EXTKEY,
	'Sqlite',
	array(
		'Sqlite' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Sqlite' => '',
		
	)
);
