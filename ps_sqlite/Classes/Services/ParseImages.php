<?php
/**
 * Created by PhpStorm.
 * User: Dragan
 * Date: 3/1/2017
 * Time: 2:15 PM
 */

namespace Peaksourcing\PsSqlite\Services;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class ParseImages
{
    public function getImages($xml_content)
    {
        $dom = new \DomDocument();
        $dom->loadHTML($xml_content);
        $all_image = $dom->getElementsByTagName('img');
        $result = array();
        foreach ($all_image as $img) {
            $result[] = $img->getAttribute('src');
        }

        return $result;
    }
}