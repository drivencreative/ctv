<?php
namespace Peaksourcing\PsSqlite\Services;

    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2016 Petar Colovic <pcolovic@peak-sourcing.com>, PeakSourcing
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * Utility
 */
class Utility
{

    /**
     * settings for extension
     **/
    public static $settings = null;

    /**
     * Settings
     *
     * @param string $key
     * @return mixed
     */
    public static function settings($key = null) {
        if (self::$settings === null) {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
            $configurationManager = $objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class);
            self::$settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'gts', 'Backoffice');
        }
        return $key ? self::$settings[$key] : self::$settings;
    }

    /**
     * Image processing
     *
     * @param integer $src
     * @param object $image
     * @param boolean $treatIdAsReference
     * @param array $settings
     *
     * @return array
     */
    public static function image($src, $image = null, $treatIdAsReference = false, $settings = []) {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        $imageService = $objectManager->get(\TYPO3\CMS\Extbase\Service\ImageService::class);
        try {
            $image = $imageService->getImage($src, $image, $treatIdAsReference);
            if (!$settings) {
                $settings = self::settings();
            }
            if ($settings['crop'] === null) {
                $settings['crop'] = $image instanceof \TYPO3\CMS\Core\Resource\FileReference ? $image->getProperty('crop') : null;
            }

            $user = '';
            if ($image->getProperty('cruser_id')) {
                $user = $objectManager->get(\Deepscreen\Gts\Domain\Repository\UserRepository::class)->findByUid($image->getProperty('cruser_id'));
                $user = trim($user->getFirstName() . ' ' . ($user->getLastName() ?: $user->getUsername()));
            }

            return [
                'url' => $imageService->getImageUri($imageService->applyProcessingInstructions($image, $settings), $settings['absolute']),
                'uid' => $image->getProperty('uid'),
                'title' => $image->getProperty('title'),
                'description' => $image->getProperty('description'),
                'alt' => $image->getProperty('alternative'),
                'link' => $image->getProperty('link'),
                'sorting' => $image->getProperty('sorting_foreign'),
                'createDate' => date('d.m.Y', $image->getProperty('crdate')),
                'createUser' => $image->getProperty('cruser_id') ? $user : ''
            ];
        } catch (\TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException $e) {
            // thrown if file does not exist
        } catch (\UnexpectedValueException $e) {
            // thrown if a file has been replaced with a folder
        } catch (\RuntimeException $e) {
            // RuntimeException thrown if a file is outside of a storage
        } catch (\InvalidArgumentException $e) {
            // thrown if file storage does not exist
        }
    }

    /**
     * Create folder if don't already exists
     *
     * @param string $folder
     *
     * @return void
     **/
    public static function createFolder($folder) {
        $uploadDir = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($folder);
        if(!is_dir($uploadDir)){
            \TYPO3\CMS\Core\Utility\GeneralUtility::mkdir_deep($uploadDir);
        }
    }

    /**
     * Pagination
     *
     * @param mixed $objects
     * @param integer $currentPage
     * @param integer $itemsPerPage
     * @param integer $count
     * @param array $configuration
     *
     * @return array
     **/
    public static function pagination($objects, $count = 0, $configuration = []) {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        $paginate = $objectManager->get(\Deepscreen\Gts\Controller\PaginateController::class);
        $paginate->setCurrentPage((int)$params['@widget_0']['currentPage'] ?: 1);
        $paginate->setWidgetConfiguration([
            'configuration' => ['itemsPerPage' => ((int)$params['itemsPerPage'] ?: 10)],
            'objects' => $objects,
            'count' => $count
        ]);
        return $paginate->getPagination($configuration);
    }

}
