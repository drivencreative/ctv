<?php

namespace Peaksourcing\PsSqlite\Services;

use \Ps\PsTables\Domain\Repository\ImageRepository;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Manifest
 */
class Manifest
{

    /**
     * The object manager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;

    /**
     * @var array
     */
    public $mergedParse = [];

    /**
     * @var \Peaksourcing\PsSqlite\Services\ParseImages
     */
    protected $parser;

    /**
     * The constructor function.
     */
    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        $this->parser = GeneralUtility::makeInstance('\Peaksourcing\PsSqlite\Services\ParseImages');
    }

    /**
     * @return mixed
     */
    public function getManifestData()
    {

        /** @var $repository \Ps\PsTables\Domain\Repository\TeaserRepository */
        $teaserRepository = $this->objectManager->get('\Ps\PsTables\Domain\Repository\TeaserRepository');

        /** @var $repository \Ps\PsTables\Domain\Repository\ImageRepository */
        $imageRepository = $this->objectManager->get('\Ps\PsTables\Domain\Repository\ImageRepository');

        /** @var $repository \Ps\PsTables\Domain\Repository\TeaserRepository */
        $page = $this->objectManager->get('\TYPO3\CMS\Frontend\Page\PageRepository');
        $teasers = $teaserRepository->findAll()->toArray();
        $homePid = $this->settings['homePid'] ? $this->settings['homePid'] : 6;

        $homeData['top_image'] = $imageRepository->findByUid($page->getPage($homePid)['homeimage'])->getImage()->getOriginalResource()->getName();

        foreach ($teasers as $teaser) {
            $homeData['tiles'][] = [
                'text' => $teaser->getName(),
                'url' => 'page://' . $teaser->getUrl(),
                'icon' => $teaser->getImage()->getImage()->getOriginalResource()->getName()
            ];
        }

        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');

        $setting = $configurationManager->getConfiguration(
            \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS
        );
        $homeData['newsRefresh'] = $setting['newsRefresh'] ? $setting['newsRefresh'] : 4;

        return $homeData;
    }

    /**
     * @param $html
     * @param $hash
     */
    public function getHtmlImages($html, $hash)
    {
        $mergedParse = [];
        $images = $this->parser->getImages($html);
        if (!empty($images)) {
//        DebuggerUtility::var_dump($images, $row['uid']);
            if ($images) {
                foreach ($images as $image) {
                    $mergedParse[] = array(
                        'file_name' => basename($image),
                        'url' => $image,
                        'hash' => $hash
                    );
                }
            }
        }
//        $this->mergedParse = $mergedParse;
        return $mergedParse;
    }
}