<?php
/**
 * Created by PhpStorm.
 * User: Dragan
 * Date: 2/3/2017
 * Time: 4:25 PM
 */

namespace Peaksourcing\PsSqlite\Services;

use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class SqliteGenerator
{

    /**
     * @var array
     */
    public $pageActions = array();

    /**
     * @var array
     */
    public $roots = array();

    /**
     * sqliteHash
     *
     * @var string
     */
    private $sqliteHash = '';

    /**
     * manifestHtmlImages
     *
     * @var string
     */
    private $manifestHtmlImages = '';

    /**
     * @var \Peaksourcing\PsSqlite\Services\Manifest
     */
    protected $manifestData;

    /**
     * @var \SQLite3
     */
    protected $database;

    /**
     * SqliteGenerator constructor.
     */
    public function __construct()
    {
        if ($_GET['create']) {
            unlink('mysqlitedb.sqlite');
        }
        $this->database = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\SQLite3', 'mysqlitedb.sqlite');
        $this->database->enableExceptions(true);
        $this->manifestData = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\Peaksourcing\PsSqlite\Services\Manifest');

    }

    /**
     * @return mixed
     */
    public function manifest()
    {
        $this->currtime = microtime(true);
        //$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;
        $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            '*',
            'tx_pssqlite_domain_model_sqlite',
            ''
        );
        //echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
        if ($GLOBALS['TYPO3_DB']->sql_num_rows($result)) {
            $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
            $this->sqliteHash = $row['hash'];
            if ($_GET['create']) {
                //if have 'create=1' parameter in URL then we create new sqlite file -> root_folder/mysqlitedb.sqlite
                if ($this->createSqlite()) {
                    $result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                        'tx_pssqlite_domain_model_sqlite',
                        'uid=' . $row['uid'],
                        array(
                            'hash' => $this->sqliteHash,
                            'manifest' => $this->manifest,
                        )
                    );
                }
            } else {
                if ($_GET['debug']) {
//                $this->printQuery();
                } else {
                    //otherwise we return manifest from cache
                    $this->manifest = $row['manifest'];
                }
            }
        } else {
            //only in case that we have no record in table tx_pssqlite_domain_model_sqlite. Theoretically, just once in life.
            if ($this->createSqlite()) {
                $result = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
                    'tx_pssqlite_domain_model_sqlite',
                    array(
                        'hash' => $this->sqliteHash,
                        'manifest' => $this->manifest,
                    )
                );
            }
        }
        return $this->manifest;
    }

    /**
     * checkTime
     *
     * @param string $section
     */
    public function checkTime($section = '')
    {
        //if we have parameter &time=1 we can check time on every critical point
        $now = microtime(true);
        if ($section && $_GET['time']) {
            echo ($now - $this->currtime) . ' ' . $section . chr(10);
        }
        $this->totalTime += $now - $this->currtime;
        $this->currtime = $now;
    }

    /**
     * writeToLog
     */
    public function writeToLog()
    {
        $myfile = fopen('logs/manifest.txt', 'a') or die('Unable to open file!');
        $txt = 'hash=' . $this->sqliteHash . ' | time=' . date('d.m.Y H:i:s') . ($this->totalTime ? (' | createTime=' . $this->totalTime) : '');
        fwrite($myfile, $txt . PHP_EOL);
        fclose($myfile);
    }

    /**
     * buildQuery - add select,
     *
     * param $tableName
     * return array
     */
    public function buildQuery($tableName, $fields = '*', $where = '')
    {
        return array(
            'table' => $tableName,
            'fields' => $fields,
            'where' => $where
        );
    }

    /**
     * printQuery
     */
    public function printQuery()
    {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        $objectManager->get('Ps\PsTables\Domain\Repository\ImageRepository')->findAllImages();
    }

    /**
     * @return bool
     */
    public function createSqlite()
    {
        //we create new hash because we create new sqlite file
        $this->sqliteHash = time();
        //create all sqlite tables we need in this project
        $this->prepareTables();
        //just checking execution time
        $this->checkTime('create tables');


        //create page Ids as strings, not integer
        $this->createPageIds($this->buildQuery(
            'pages',
            'uid, pid, title, sorting, ctvcontent, ctvcontent2, ctvtype, ctvimage, ctvbeacons',
            'not hidden AND not deleted AND doktype=1'
        ));

        //menu, page tables
        $sql = $this->getZMENUandZPAGEandZ_8PAGE($this->buildQuery(
            'pages',
            'uid, pid, title, sorting, ctvcontent, ctvcontent2, ctvtype, ctvimage, ctvbeacons, tstamp',
            'not hidden AND not deleted AND doktype=1'
        ));
        $this->database->exec($sql);
        $this->checkTime('menu, pages');

        //image table
        $sql = $this->getZIMAGE($this->buildQuery(
            'tx_pstables_domain_model_image a, sys_file_reference b, sys_file c',
            'a.uid, a.pid, a.tstamp, a.link, c.identifier as image',
            'not a.hidden AND not a.deleted AND not b.hidden AND not b.deleted AND b.tablenames = \'tx_pstables_domain_model_image\' AND b.uid_foreign = a.uid AND b.uid_local = c.uid'
        ));
        $this->database->exec($sql);
        $this->checkTime('image');

        // BANER
        $sql = $this->getZBANNER($this->buildQuery(
            'tx_pstables_domain_model_banner',
            '*',
            'not hidden AND not deleted'
        ));

        $this->database->exec($sql);
        $this->checkTime('banner');

        //COMPANY
        $sql = $this->getZCOMPANYandZ_3IMAGE($this->buildQuery(
            'tx_pstables_domain_model_company',
            '*',
            'not hidden AND not deleted'
        ));
        $this->database->exec($sql);
        $this->checkTime('company');

        //EVENT
        $sql = $this->getZEVENTandZ_5IMAGEandZ_3EVENT($this->buildQuery(
            'tx_pstables_domain_model_event',
            '*',
            'not hidden AND not deleted'
        ));
        $this->database->exec($sql);
        $this->checkTime('event');

        //BEACON
        $sql = $this->getZBEACON($this->buildQuery(
            'tx_pstables_domain_model_beacon',
            '*',
            'not hidden AND not deleted'
        ));
        $this->database->exec($sql);
        $this->checkTime('beacon');

        //CONF
        $sql = $this->getZCONFIG($this->buildQuery(
            'tx_pstables_domain_model_conf',
            '*',
            'not hidden AND not deleted'
        ));
        $this->database->exec($sql);
        $this->checkTime('conf');

        //COMPANY CATEGORY
        $sql = $this->getZCOMPANYCATEGORY($this->buildQuery(
            'tx_pstables_domain_model_companycategory',
            '*',
            'not hidden AND not deleted'
        ));
        $this->database->exec($sql);
        $this->checkTime('companycategory');

        //NEWS
        $sql = $this->getZNEWS($this->buildQuery(
            'tx_news_domain_model_news',
            '*',
            'not hidden AND not deleted'
        ));
        $this->database->exec($sql);
        $this->checkTime('companycategory');

        //COMPANY-CATEGORY RELATIONS
        $sql = $this->getZCOMPANYCATEGORYmm($this->buildQuery(
            'tx_pstables_company_companycategory_mm',
            'uid_local, uid_foreign',
            ''
        ));
        $this->database->exec($sql);
        $this->checkTime('companycategory_mm');


        $this->manifest = array(
            'manifest' => array(
                0 => array(
                    'file_name' => 'mysqlitedb.sqlite',
                    'url' => 'http://' . $_SERVER['SERVER_NAME'] . '/mysqlitedb.sqlite',
                    'hash' => '' . $this->sqliteHash . ''
                ),
            ),
        );

        $this->manifest['manifest'] = array_merge($this->manifest['manifest'], $this->manifestImg, $this->manifestHtmlImages);
        $this->manifest = json_encode($this->manifest, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        $this->writeToLog();

        return true;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZBEACON($table)
    {
        #ZBEACON

        $rows = $this->getTables($table);
        foreach ($rows as $row) {
            $sql .= 'INSERT INTO ZBEACON (Z_PK,Z_ENT,Z_OPT,ZVISITED,ZBEACONID,ZLINK,ZMAJOR,ZMINOR,ZNAME,ZTEXT,ZUUID)
			VALUES (' . $row['uid'] . ',
			2,
			1,
			0,
			\'' . $row['uid'] . '\',
			\'' . (intval($row['link']) ? 'page://' . $this->pageActions[$row['link']] : $row['link']) . '\',
			\'' . $row['major'] . '\',
			\'' . $row['minor'] . '\',
			\'' . $row['title'] . '\',
			\'' . $this->createAppLinks($row['content']) . '\',
			\'' . $row['uuid'] . '\');' . chr(10);
        }
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZEVENTandZ_5IMAGEandZ_3EVENT($table)
    {
        $rows = $this->getTables($table);
        foreach ($rows as $row) {
            #ZEVENT
            $sql .= 'INSERT INTO ZEVENT (Z_PK,Z_ENT,Z_OPT,ZEVENTID,ZLOCATION,ZSPEAKERFUNCTION,ZSPEAKERPOSITION,ZSPEAKERS,ZTEXT,ZTIME,ZTITLE)
			VALUES (
			' . $row['uid'] . ',
			5,
			1,
			\'event_' . $row['uid'] . '\',
			\'' . $row['location'] . '\',
			\'' . $row['speakerfunction'] . '\',
			\'' . $row['speakerposition'] . '\',
			\'' . $row['speakers'] . '\',
			\'' . $this->createAppLinks($row['content']) . '\',
			\'' . date('Y/m/d', $row['start']) . ' ' . date('H:i', $row['start']) . '-' . date('H:i', $row['end']) . '\',
			\'' . $row['title'] . '\');' . chr(10);

            #Z_6IMAGE CREATE TABLE Z_6IMAGE ( Z_6EVENT1 INTEGER, Z_8IMAGE1 INTEGER, PRIMARY KEY (Z_6EVENT1, Z_8IMAGE1) )
            if ($row['image']) {
                $sql .= 'INSERT INTO Z_6IMAGE (Z_6EVENT1,Z_8IMAGE1)
				VALUES (
				\'' . $row['uid'] . '\',
				\'' . $row['image'] . '\');' . chr(10);
            }

            #Z_3EVENT CREATE TABLE Z_4EVENT ( Z_4COMPANY1 INTEGER, Z_6EVENT INTEGER, PRIMARY KEY (Z_4COMPANY1, Z_6EVENT) )
            $sql .= 'INSERT INTO Z_4EVENT (Z_4COMPANY1,Z_6EVENT)
			VALUES (
			\'' . $row['company'] . '\',
			\'' . $row['uid'] . '\');' . chr(10);
        }
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZCOMPANYandZ_3IMAGE($table)
    {
        $rows = $this->getTables($table);

        foreach ($rows as $row) {
            #ZCOMPANY
            $sql .= 'INSERT INTO ZCOMPANY (Z_PK,Z_ENT,Z_OPT,ZCOMPANYID,ZLOCATION,ZNAME)	
			VALUES (
			' . $row['uid'] . ',
			4,
			1,
			\'company_' . $row['uid'] . '\',
			\'' . $row['location'] . '\',
			\'' . $row['title'] . '\');' . chr(10);

            #Z_4IMAGE  CREATE TABLE Z_4IMAGE ( Z_4COMPANY INTEGER, Z_8IMAGE INTEGER, PRIMARY KEY (Z_4COMPANY, Z_8IMAGE) )
            if ($row['image']) {
                $sql .= 'INSERT INTO Z_4IMAGE (Z_4COMPANY,Z_8IMAGE)			
				VALUES (
				\'' . $row['uid'] . '\',
				\'' . $row['image'] . '\');' . chr(10);
            }
        }
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZCOMPANYCATEGORY($table)
    {
        $rows = $this->getTables($table);

        foreach ($rows as $row) {
            #ZCATEGORY
            $sql .= 'INSERT INTO ZCATEGORYCOMPANY (Z_PK,Z_ENT,Z_OPT,ZNAME)	
			VALUES (
			' . $row['uid'] . ',
			3,
			1,
			\'' . $row['title'] . '\'
			);' . chr(10);
        }
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZNEWS($table)
    {
        $rows = $this->getTables($table);
        foreach ($rows as $row) {
            #ZCATEGORY
            $sql .= 'INSERT INTO ZNEWS (Z_PK,Z_ENT,Z_OPT,ZEND,ZSTART,ZSUMMARY,ZTITLE,ZURL)	
			VALUES (
			' . $row['uid'] . ',
			10,
			1,
			\'' . $row['endtime'] . '\',
			\'' . $row['starttime'] . '\',
			\'' . $row['teaser'] . '\',
			\'' . $row['title'] . '\',
			\'' . $row['externalurl'] . '\');' . chr(10);
        }
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZCOMPANYCATEGORYmm($table)
    {
        //print_r($table);die();
        $rows = $this->getTables($table);
        foreach ($rows as $row) {
            $sql .= 'INSERT INTO Z_3COMPANIES (Z_3CATEGORIES,Z_4COMPANIES)
            VALUES (
            ' . $row['uid_foreign'] . ',
            ' . $row['uid_local'] . '
            );' . chr(10);

        }
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZBANNER($table)
    {
        #ZBANNER
        $rows = $this->getTables($table);
        foreach ($rows as $row) {
            //Set global position
            $row['position'] = $row['position_record_type'] ? $row['position_event'] : $row['position_company'];
            $url = intval($row['url']) ? $url = 'page://' . $this->pageActions[$row['url']] : $url = $row['url'];
            $sql .= 'INSERT INTO ZBANNER (Z_PK,Z_ENT,Z_OPT,ZIMAGE,ZEND,ZSTART,ZBANNERID,ZPAGEID,ZPOSITION,ZSIZE,ZTYPE,ZURL)	
			VALUES (' . $row['uid'] . ',
			1,
			1,
			\'' . $row['image'] . '\',
			\'' . $row['end_time'] . '\',
			\'' . $row['start_time'] . '\',
			\'banner_' . $row['uid'] . '\',
			\'' . $this->pageActions[$row['page']] . '\',
			\'' . $row['position'] . '\',
			\'' . $row['size'] . '\',
			\'' . $row['btype'] . '\',
			\'' . $url . '\');' . chr(10);
        }
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZIMAGE($table)
    {
        //ZIMAGE
        $rows = $this->getTables($table);
        foreach ($rows as $key => $row) {
            $row['image'] = 'fileadmin' . $row['image'];
            $file_name = substr($row['image'], (strrpos($row['image'], '/') + 1));
            if (!in_array($file_name, $this->file_names)) {
                $this->file_names[] = $file_name;
                $this->manifestImg[] = array(
                    'file_name' => $file_name,
                    'url' => 'http://' . $_SERVER['SERVER_NAME'] . '/' . $row['image'],
                    'hash' => '' . $row['tstamp'] . ''
                );
            }
            $rows[$key]['file_name'] = $file_name;
            $rows[$key]['link'] = intval($row['link']) ? 'index.php?id=' . $row['link'] : $row['link'];
        }
        foreach ($rows as $row) {
            $sql .= 'INSERT INTO ZIMAGE (Z_PK,Z_ENT,Z_OPT,ZNAME,ZURL) 
			VALUES (' . $row['uid'] . ',
			7,
			1,
			\'' . $row['file_name'] . '\',
			\'' . $row['link'] . '\');' . chr(10);
        }
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZMENUandZPAGEandZ_8PAGE($table)
    {
        //PAGES
        $rows = $this->getTables($table);
        $this->pageRows = $rows;
        $this->setLevel(1, 0);
        $rows = $this->pageRows;
        $arrayImage = [];
        foreach ($rows as $row) {
            if ($row['pid'] != 0) {
                $action = $this->pageActions[$row['uid']];

                #ZMENU
                if ($row['uid'] != $this->settings['loginPid']) {
                    $sql .= 'INSERT INTO ZMENU (Z_PK,Z_ENT,Z_OPT,ZLEVEL,ZORDER,ZIMAGE,ZPARENT,ZACTIONID,ZMENUID,ZTITLE) 

					VALUES (
					' . $row['uid'] . ',
					8,
					1,
					\'' . ($row['level'] ? $row['level'] : '0') . '\',
					\'' . $row['sorting'] . '\',
					\'\',
					\'' . ($row['level'] ? $row['pid'] : 'NULL') . '\',
					\'' . $action . '\',
					\'' . $action . '\',
					\'' . $row['title'] . '\');' . chr(10);
                }
                #ZPAGE
                if ($row['ctvtype'] == '') {
                    $row['ctvtype'] = 'page';
                }
                if ($row['uid'] == $this->settings['myProfilPid'] || $row['uid'] == $this->settings['loginPid']) {
                    //some pages need special form of content
                    $content = '{"first_section" : "' . $this->cleanString($row['ctvcontent']) . '", "second_section" : "' . $this->cleanString($row['ctvcontent2']) . '"}';
                } else {
                    $content = $row['ctvcontent'];
                }
                $homePid = $this->settings['homePid'] ? $this->settings['homePid'] : 6;
                $content = $row['uid'] == $homePid ? json_encode($this->manifestData->getmanifestData(),
                    JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT) : $this->createAppLinks($content);
//                DebuggerUtility::var_dump($row);die;
                //Add <img src images to manifest
                $arrayImage = array_merge($arrayImage, $this->manifestData->getHtmlImages($row['ctvcontent'], (string)$row['tstamp']));

                if (!in_array($row['uid'], $this->roots)) {
                    $sql .= 'INSERT INTO ZPAGE (Z_PK,Z_ENT,Z_OPT,ZPAGEID,ZTEXT,ZTITLE,ZTYPE)
					VALUES (
					' . $row['uid'] . ',
					9,
					1,
					\'' . $action . '\',
					\'' . $content . '\',
					\'' . $row['title'] . '\',
					\'' . $row['ctvtype'] . '\');' . chr(10);

                    if ($row['ctvimage'] != 0) {
                        $sql .= 'INSERT INTO Z_8PAGE (Z_8IMAGE2,Z_11PAGE)
						VALUES (
						\'' . $row['ctvimage'] . '\',
						\'' . $row['uid'] . '\');' . chr(10);
                    }
                }
            }
        }
        $this->manifestHtmlImages = $arrayImage;
        return $sql;
    }

    /**
     * @param $table
     * @return string
     */
    public function getZCONFIG($table)
    {
        #ZCONFIG
        $rows = $this->getTables($table);
        $row = $rows[0];

        $sql = 'INSERT INTO ZCONFIG (Z_PK,Z_ENT,Z_OPT,ZNUMBER_OF_BEACONS_REQUIRED)
		VALUES (
		1,
		4,
		1,
		\'4\');' . chr(10);
        return $sql;
    }

    /* library functions */

    /**
     * createAppLinks - modifies internal links for app. Example <a href="http://ctv.peak-sourcing.com/?id=9">go here</a> will be <a href="page://meine_favoriten">go here</a>
     *
     * @param $source
     * @return mixed
     */
    public function createAppLinks($source)
    {
        //$source = 'an der <a href="http://ctv.peak-sourcing.com/?id=9" class="internal-link" title="Opens internal link in current window">Jahrhunderthalle </a>dinieren';
        $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
        if (preg_match_all("/$regexp/siU", $source, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                if (strpos($match[2], $_SERVER['SERVER_NAME']) !== false) {
                    $linkParts = explode('id=', $match[2]);
                    if (intval($linkParts[1])) {
                        $newATag = '<a href="page://' . $this->pageActions[$linkParts[1]] . '">' . $match[3] . '</a>';
                        $source = str_replace($match[0], $newATag, $source);
                    }
                }
            }
        }
        return $source;
    }

    /**
     * createPageIds - create page Ids as strings, not integer
     *
     * @param $table
     * @return array
     */
    public function createPageIds($table)
    {

        $rows = $this->getTables($table);
        $searchChars = array('ä', 'Ä', 'ö', 'Ö', 'ü', 'Ü', 'ß', ' ', '&');
        $replaceChars = array('ae', 'ae', 'oe', 'oe', 'ue', 'ue', 'ss', '_', '_');
        foreach ($rows as $row) {
            $action = strtolower($row['title']);
            if ($action == 'datenschutzerklärung') {
                $action = 'datenschutz';
            }
            $action = str_replace($searchChars, $replaceChars, $action);
            $this->pageActions[$row['uid']] = $action;
        }
        return $this->pageActions;
    }

    /**
     * cleanString
     *
     * @param $content
     * @return string
     */
    public function cleanString($content)
    {
        $content = str_replace('<p>', '', $content);
        $content = str_replace('</p>', '\\n', $content);
        $content = str_replace('&nbsp;', ' ', $content);
        //remove last \n if it is on the end of $content
        if ((strlen($content) - strrpos($content, '\\n')) == 2) {
            $content = substr($content, 0, -2);
        }
        return addslashes($content);
    }

    /**
     * ZMENU table need info about level of page related to root. (0, 1, 2....)
     *
     * @param $uid
     * @param $level
     */
    public function setLevel($uid, $level)
    {
        foreach ($this->pageRows as $key => $row) {
            if ($row['pid'] == $uid) {
                $this->roots[] = $row['pid'];
                $this->pageRows[$key]['level'] = $level;
                $this->setLevel($row['uid'], ($level + 1));
            }
        }
    }

    //in the moment not needed
    public function addQuotes($rows)
    {
        foreach ($rows as $key1 => $row) {
            foreach ($row as $key2 => $value) {
                $rows[$key1][$key2] = '\'' . $value . '\'';
            }
        }
        return $rows;
    }

    //basic typo3 mysql select query
    public function getTables($table)
    {

        $rows = array();
        //$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;
        $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            $table['fields'],
            $table['table'],
            $table['where']
        );
        //echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
        while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Array flatten
     *
     * @param $array
     * @return array
     */
    public function array_flatten($array)
    {

        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $return = array_merge($return, array_flatten($value));
            } else {
                $return[$key] = $value;
            }
        }
        return $return;

    }

    /**
     * creates all needed sqlite tables for this project
     */
    public function prepareTables()
    {
        $sql = "
			BEGIN TRANSACTION;
			CREATE TABLE Z_PRIMARYKEY (Z_ENT INTEGER PRIMARY KEY, Z_NAME VARCHAR, Z_SUPER INTEGER, Z_MAX INTEGER);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (1,'Banner',0,3);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (2,'Beacon',0,10);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (3,'Categories',0,1);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (4,'Company',0,1);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (7,'Config',0,1);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (5,'Favorite',0,18);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (6,'Event',0,1);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (8,'Image',0,3);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (9,'Menu',0,0);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (11,'Page',0,2);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (10,'News',0,2);
			
            CREATE TABLE ZBANNER ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZIMAGE INTEGER, ZEND TIMESTAMP, ZSTART TIMESTAMP, ZBANNERID VARCHAR, ZPAGEID VARCHAR, ZPOSITION VARCHAR, ZSIZE VARCHAR, ZTYPE VARCHAR, ZURL VARCHAR );
            
            CREATE TABLE ZBEACON ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZVISITED INTEGER, ZBEACONID VARCHAR, ZLINK VARCHAR, ZMAJOR VARCHAR, ZMINOR VARCHAR, ZNAME VARCHAR, ZTEXT VARCHAR, ZUUID VARCHAR );
            
            CREATE TABLE ZCATEGORYCOMPANY ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZNAME VARCHAR );
            
            CREATE TABLE ZCOMPANY ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZCOMPANYID VARCHAR, ZLOCATION VARCHAR, ZNAME VARCHAR );
            
            CREATE TABLE ZCONFIG ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZNUMBER_OF_BEACONS_REQUIRED INTEGER );
            
            CREATE TABLE ZEVENT ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZEVENTID VARCHAR, ZLOCATION VARCHAR, ZSPEAKERFUNCTION VARCHAR, ZSPEAKERPOSITION VARCHAR, ZSPEAKERS VARCHAR, ZTEXT VARCHAR, ZTIME VARCHAR, ZTITLE VARCHAR );
            
            CREATE TABLE ZFAVORITE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZOBJECTID VARCHAR, ZTYPE VARCHAR );
            
            CREATE TABLE ZIMAGE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZNAME VARCHAR, ZURL VARCHAR );
            
            CREATE TABLE ZMENU ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZLEVEL INTEGER, ZORDER INTEGER, ZIMAGE INTEGER, ZPARENT INTEGER, ZACTIONID VARCHAR, ZMENUID VARCHAR, ZTITLE VARCHAR );
            
            CREATE TABLE ZNEWS ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZEND TIMESTAMP, ZSTART TIMESTAMP, ZSUMMARY VARCHAR, ZTITLE VARCHAR, ZURL VARCHAR );
            
            CREATE TABLE ZPAGE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZPAGEID VARCHAR, ZTEXT VARCHAR, ZTITLE VARCHAR, ZTYPE VARCHAR );

            CREATE TABLE Z_3COMPANIES ( Z_3CATEGORIES INTEGER, Z_4COMPANIES INTEGER, PRIMARY KEY (Z_3CATEGORIES, Z_4COMPANIES) );

                        
            CREATE TABLE Z_4EVENT ( Z_4COMPANY1 INTEGER, Z_6EVENT INTEGER, PRIMARY KEY (Z_4COMPANY1, Z_6EVENT) );
            
            CREATE TABLE Z_4IMAGE ( Z_4COMPANY INTEGER, Z_8IMAGE INTEGER, PRIMARY KEY (Z_4COMPANY, Z_8IMAGE) );
            
            CREATE TABLE Z_6IMAGE ( Z_6EVENT1 INTEGER, Z_8IMAGE1 INTEGER, PRIMARY KEY (Z_6EVENT1, Z_8IMAGE1) );
            
            CREATE TABLE Z_8PAGE ( Z_8IMAGE2 INTEGER, Z_11PAGE INTEGER, PRIMARY KEY (Z_8IMAGE2, Z_11PAGE) );
            
            CREATE TABLE Z_METADATA (Z_VERSION INTEGER PRIMARY KEY, Z_UUID VARCHAR(255), Z_PLIST BLOB);
            
            CREATE TABLE Z_MODELCACHE (Z_CONTENT BLOB);
            
            CREATE INDEX ZBANNER_ZIMAGE_INDEX ON ZBANNER (ZIMAGE);
            
            CREATE INDEX ZMENU_ZPARENT_INDEX ON ZMENU (ZPARENT);
            
            CREATE INDEX Z_4EVENT_Z_6EVENT_INDEX ON Z_4EVENT (Z_6EVENT, Z_4COMPANY1);
            
            CREATE INDEX Z_4IMAGE_Z_8IMAGE_INDEX ON Z_4IMAGE (Z_8IMAGE, Z_4COMPANY);
            
            CREATE INDEX Z_6IMAGE_Z_8IMAGE1_INDEX ON Z_6IMAGE (Z_8IMAGE1, Z_6EVENT1);
            
            CREATE INDEX Z_8PAGE_Z_11PAGE_INDEX ON Z_8PAGE (Z_11PAGE, Z_8IMAGE2);
            
			COMMIT;
		";
        $this->database->exec($sql);
    }

}