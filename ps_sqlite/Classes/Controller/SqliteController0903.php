<?php
namespace Peaksourcing\PsSqlite\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * SqliteController
 */
class SqliteController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * sqliteRepository
	 *
	 * @var \Peaksourcing\PsSqlite\Domain\Repository\SqliteRepository
	 * @inject
	 */
	protected $sqliteRepository = NULL;
	
	public $pageActions = array();
	public $roots = array();

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {		
		$this->currtime = microtime(true);
		//$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;	
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'tx_pssqlite_domain_model_sqlite',
			''
		);
		//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;

		if ($GLOBALS['TYPO3_DB']->sql_num_rows($result)) {     
			$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
			$this->sqliteHash = $row['hash'];			
			if($_GET['create']){
				if($this->updateSqlite()){	
					$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
						'tx_pssqlite_domain_model_sqlite',
						'uid=' . $row['uid'],
						array(
							'hash' => $this->sqliteHash,
							'manifest' => $this->manifest,
						)
					);
				}
			} else {
				$this->manifest = $row['manifest'];
			}
		} else {			
			if($this->updateSqlite()){
				$result = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
					'tx_pssqlite_domain_model_sqlite',
					array(
						'hash' => $this->sqliteHash,
						'manifest' => $this->manifest,
					)
				);
			}
		}
		  
		$this->view->assign('response', $this->manifest);
		  
		  
						############################
						#### TEST ##################
						############################
						
						$this->database = new \SQLite3('mysqlitedb.sqlite');	
						$sql = 'SELECT * FROM ZEVENT';	
						//$sql = 'SHOW TABLES';	
						$rows = $this->database->query($sql);
						$results = array();
						if($rows) {
							// make assoc array
							while ($row = $rows->fetchArray(SQLITE3_ASSOC)) {
								$results[] = $row;
								//echo $row['ZACTIONID'] . chr(10);
							}
						}
						//print_r($results);	
						
						##############################
						#### TEST ####################
						##############################		  
	}
	
	public function checkTime($section = ''){	
		$now = microtime(true);
		if($section && $_GET['time']){			
			echo ($now - $this->currtime) . ' ' . $section . chr(10);
		} 
		$this->totalTime += $now - $this->currtime;
		$this->currtime = $now;		
	}
	
	public function writeToLog(){
		$myfile = fopen('logs/manifest.txt', 'a') or die('Unable to open file!');
		$txt = 'hash=' . $this->sqliteHash . ' | time=' . date('d.m.Y H:i:s') . ($this->totalTime ? (' | createTime=' . $this->totalTime) : '');  
		fwrite($myfile, $txt . PHP_EOL);
		fclose($myfile);
	}
	
	public function updateSqlite(){	
		//mb_internal_encoding('UTF-8');
		//mb_convert_encoding($row['title'], 'UTF-8')
		//header('Content-Type: application/json; charset=utf-8');
		$this->sqliteHash = time();	
		$this->prepareTables();
		$this->checkTime('create tables');
		$sql = '';
		
		$tables = array(
			
			0 => array(
				'table' => 'pages',
				'fields' => 'uid, pid, title, sorting, ctvcontent, ctvcontent2, ctvtype, ctvimage, ctvbeacons',
				'where' => 'not hidden AND not deleted AND doktype=1'
			),
			
			1 => array(
				'table' => 'tx_pstables_domain_model_image a, sys_file_reference b, sys_file c',
				'fields' => 'a.uid, a.pid, a.tstamp, a.link, c.identifier as image',
				'where' => 'not a.hidden AND not a.deleted AND not b.hidden AND not b.deleted AND b.tablenames = \'tx_pstables_domain_model_image\' AND b.uid_foreign = a.uid AND b.uid_local = c.uid',
			),
			
			2 => array(
				'table' => 'tx_pstables_domain_model_banner',
				'fields' => '*',
				'where' => 'not hidden AND not deleted'
			),
			
			3 => array(
				'table' => 'tx_pstables_domain_model_company',
				'fields' => '*',
				'where' => 'not hidden AND not deleted'
			),
			
			4 => array(
				'table' => 'tx_pstables_domain_model_event',
				'fields' => '*',
				'where' => 'not hidden AND not deleted'
			),
			
			5 => array(
				'table' => 'tx_pstables_domain_model_beacon',
				'fields' => '*',
				'where' => 'not hidden AND not deleted'
			),
			
			6 => array(
				'table' => 'tx_pstables_domain_model_conf',
				'fields' => '*',
				'where' => 'not hidden AND not deleted'
			),
		);
		
		$this->createPageIds($tables[0]);
		
		$sql = $this->getZMENUandZPAGEandZ_7PAGE($tables[0]);
		$this->database->exec($sql);
		$this->checkTime('menu, pages');
		
		$sql = $this->getZIMAGE($tables[1]);
		$this->database->exec($sql);	
		$this->checkTime('image');
		
		$sql = $this->getZBANNER($tables[2]);		
		$this->database->exec($sql);
		$this->checkTime('banner');
		
		$sql = $this->getZCOMPANYandZ_3IMAGE($tables[3]);		
		$this->database->exec($sql);
		$this->checkTime('company');
		
		$sql = $this->getZEVENTandZ_5IMAGEandZ_3EVENT($tables[4]);		
		$this->database->exec($sql);
		$this->checkTime('event');
		
		$sql = $this->getZBEACON($tables[5]);		
		$this->database->exec($sql);
		$this->checkTime('beacon');
		
		$sql = $this->getZCONFIG($tables[6]);		
		$this->database->exec($sql);
		$this->checkTime('conf');
		//print_r($sql);
		
		$this->manifest = array(
			'manifest' => array(
				0 => array(
					'file_name' => 'mysqlitedb.sqlite',
					'url' => 'http://' . $_SERVER['SERVER_NAME'] . '/mysqlitedb.sqlite',
					'hash' => '' . $this->sqliteHash . ''
				),
			),
		);
		$this->manifest['manifest'] = array_merge($this->manifest['manifest'], $this->manifestImg);
		$this->manifest = json_encode($this->manifest, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
		$this->writeToLog();
		
		return TRUE;
	}
	
	public function getZBEACON($table){
		#ZBEACON
		$rows = $this->getTables($table);
		foreach($rows as $row){
			$sql .= 'INSERT INTO ZBEACON (Z_PK,Z_ENT,Z_OPT,ZVISITED,ZBEACONID,ZNAME,ZTEXT,ZLINK,ZMAJOR,ZMINOR,ZUUID)
			VALUES (' . $row['uid'] . ',
			2,
			1,
			0,
			\'' . $row['uid'] . '\',
			\'' . $row['title'] . '\',
			\'' . $this->createAppLinks($row['content']) . '\',
			\'' . (intval($row['link']) ? 'page://' . $this->pageActions[$row['link']] : $row['link']) . '\',
			\'' . $row['major'] . '\',
			\'' . $row['minor'] . '\',
			\'' . $row['uuid'] . '\');' . chr(10);		
		}	
		//print_r($sql);die();
		return $sql;
	}
	
	public function getZEVENTandZ_5IMAGEandZ_3EVENT($table){
		$rows = $this->getTables($table);
		foreach($rows as $row){
			#ZEVENT
			//(1,4,1,'testEvent','room #42',NULL,'Speaker1, Speaker 2, Speaker 3','some loerm ipsum ore lorem ipsum and some more','11:00 - 12:00','Some event');
			$sql .= 'INSERT INTO ZEVENT (Z_PK,Z_ENT,Z_OPT,ZEVENTID,ZLOCATION,ZSPEAKERPOSITION,ZSPEAKERFUNCTION,ZSPEAKERS,ZTEXT,ZTIME,ZTITLE)
			VALUES (
			' . $row['uid'] . ',
			5,
			1,
			\'event_' . $row['uid'] . '\',
			\'' . $row['location'] . '\',
			\'' . $row['speakerposition'] . '\',
			\'' . $row['speakerfunction'] . '\',
			\'' . $row['speakers'] . '\',
			\'' . $this->createAppLinks($row['content']) . '\',
			\'' . date('Y/m/d', $row['start']) . ' ' . date('H:i', $row['start']) . '-' . date('H:i', $row['end']) . '\',
			\'' . $row['title'] . '\');' . chr(10);		
			
			#Z_5IMAGE
			if($row['image']){
				$sql .= 'INSERT INTO Z_5IMAGE (Z_5EVENT1,Z_7IMAGE1)
				VALUES (
				\'' . $row['uid'] . '\',
				\'' . $row['image'] . '\');' . chr(10);	
			}			
			
			#Z_3EVENT
			$sql .= 'INSERT INTO Z_3EVENT (Z_3COMPANY,Z_5EVENT)
			VALUES (
			\'' . $row['company'] . '\',
			\'' . $row['uid'] . '\');' . chr(10);	
		}	
		//print_r($sql);die();
		return $sql;
	}
	
	public function getZCOMPANYandZ_3IMAGE($table){
		$rows = $this->getTables($table);
		foreach($rows as $row){
			#ZCOMPANY
			//VALUES (1,3,1,'testCompany','11, 12, 13, 14','Some company name');
			$sql .= 'INSERT INTO ZCOMPANY (Z_PK,Z_ENT,Z_OPT,ZCOMPANYID,ZLOCATION,ZNAME)	
			VALUES (' . $row['uid'] . ',
			3,
			1,
			\'company_' . $row['uid'] . '\',
			\'' . $row['location'] . '\',
			\'' . $row['title'] . '\');' . chr(10);	

			#Z_3IMAGE
			if($row['image']){
				$sql .= 'INSERT INTO Z_3IMAGE (Z_3COMPANY1,Z_7IMAGE)			
				VALUES (
				\'' . $row['uid'] . '\',
				\'' . $row['image'] . '\');' . chr(10);	
			}
		}	
		//print_r($sql);die();
		return $sql;
	}

	public function getZBANNER($table){
		#ZBANNER
		$rows = $this->getTables($table);
		foreach($rows as $row){
			if(intval($row['url'])){
				$url = 'page://' . $this->pageActions[$row['url']];
			} else {
				$url = $row['url'];
			}
			
			$sql .= 'INSERT INTO ZBANNER (Z_PK,Z_ENT,Z_OPT,ZIMAGE,ZSTART,ZEND,ZBANNERID,ZPAGEID,ZPOSITION,ZSIZE,ZTYPE,ZURL)	
			VALUES (' . $row['uid'] . ',
			1,
			1,
			\'' . $row['image'] . '\',
			\'' . $row['start_time'] . '\',
			\'' . $row['end_time'] . '\',
			\'banner_' . $row['uid'] . '\',
			\'' . $this->pageActions[$row['page']] . '\',
			\'' . $row['position'] . '\',
			\'' . $row['size'] . '\',
			\'' . $row['btype'] . '\',
			\'' . $url . '\');' . chr(10);		
		}	
		//print_r($sql);die();
		return $sql;
	}	

	public function getZIMAGE($table){
		//ZIMAGE		
		$rows = $this->getTables($table);
		foreach($rows as $key => $row){
			$row['image'] = 'fileadmin' . $row['image'];
			$file_name = substr($row['image'], (strrpos($row['image'], '/') + 1));
			if(!in_array($file_name, $this->file_names)){
				$this->file_names[] = $file_name;
				$this->manifestImg[] = array(
					'file_name' => $file_name,
					'url' => 'http://' . $_SERVER['SERVER_NAME'] . '/' . $row['image'],
					'hash' => '' . $row['tstamp'] . ''
				);				
			}
			$rows[$key]['file_name'] = $file_name;
			$rows[$key]['link'] = intval($row['link']) ? 'index.php?id=' . $row['link'] : $row['link'];
		}
		//$rows = $this->addQuotes($rows);
		foreach($rows as $row){
			$sql .= 'INSERT INTO ZIMAGE (Z_PK,Z_ENT,Z_OPT,ZNAME,ZURL) 
			VALUES (' . $row['uid'] . ',
			7,
			1,
			\'' . $row['file_name'] . '\',
			\'' . $row['link'] . '\');' . chr(10);		
		}	
		//print_r($sql);die();
		return $sql;
	}

	public function getZMENUandZPAGEandZ_7PAGE($table) {
		//PAGES
		$rows = $this->getTables($table);
		$this->pageRows = $rows;
		$this->setLevel(1, 0);
		$rows = $this->pageRows;
		foreach($rows as $row){
			if($row['pid'] != 0){
				$action = $this->pageActions[$row['uid']];
				
				#ZMENU
				if($row['uid'] != $this->settings['loginPid']){			
					$sql .= 'INSERT INTO ZMENU (Z_PK,Z_ENT,Z_OPT,ZLEVEL,ZORDER,ZPARENT,ZACTIONID,ZMENUID,ZTITLE) 
					VALUES (
					' . $row['uid'] . ',
					8,
					1,
					\'' . ($row['level'] ? $row['level'] : '0') . '\',
					\'' . $row['sorting'] . '\',
					\'' . ($row['level'] ? $row['pid'] : 'NULL') . '\',
					\'' . $action . '\',
					\'' . $action . '\',
					\'' . $row['title'] . '\');' . chr(10);	
				}
				
				#ZPAGE			
				if($row['ctvtype'] == ''){
					$row['ctvtype'] = 'page';
				}
				if($row['uid'] == $this->settings['myProfilPid'] || $row['uid'] == $this->settings['loginPid']){					
					$content = '{"first_section" : "' . $this->cleanString($row['ctvcontent']) . '", "second_section" : "' . $this->cleanString($row['ctvcontent2']) . '"}';
				} else {
					$content = $row['ctvcontent'];
				}
				
				$content = $this->createAppLinks($content);
				
				if(!in_array($row['uid'], $this->roots)){
					$sql .= 'INSERT INTO ZPAGE (Z_PK,Z_ENT,Z_OPT,ZPAGEID,ZTEXT,ZTITLE,ZTYPE)
					VALUES (
					' . $row['uid'] . ',
					9,
					1,
					\'' . $action . '\',
					\'' . $content . '\',
					\'' . $row['title'] . '\',
					\'' . $row['ctvtype'] . '\');' . chr(10);	
					
					#Z_7PAGE	
					if($row['ctvimage'] != 0){					
						$sql .= 'INSERT INTO Z_7PAGE (Z_7IMAGE2,Z_9PAGE)
						VALUES (
						\'' . $row['ctvimage'] . '\',
						\'' . $row['uid'] . '\');' . chr(10);	
					}					
				}
			} 
		} 
				
		//print_r($sql);die();
		return $sql;
	}
		
	public function getZCONFIG($table){
		#ZCONFIG
		$rows = $this->getTables($table);
		$row = $row[0];
		$sql .= 'INSERT INTO ZCONFIG (Z_PK,Z_ENT,Z_OPT,ZNUMBER_OF_BEACONS_REQUIRED)
		VALUES (
		1,
		4,
		1,
		\'' . $row['beaconsrequired'] . '\');' . chr(10);	
		//print_r($sql);die();
		return $sql;
	}
	
	
	
	/* library functions */
	
	public function createAppLinks($source){
		//$source = 'an der <a href="http://ctv.peak-sourcing.com/?id=9" class="internal-link" title="Opens internal link in current window">Jahrhunderthalle </a>dinieren';
		$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
		if(preg_match_all("/$regexp/siU", $source, $matches, PREG_SET_ORDER)) {				
			foreach($matches as $match) {
				if(strpos($match[2], $_SERVER['SERVER_NAME']) !== false) {
					$linkParts = explode('id=', $match[2]);
					if(intval($linkParts[1])){
						$newATag = '<a href="page://' . $this->pageActions[$linkParts[1]] . '">' . $match[3] . '</a>';
						$source = str_replace($match[0], $newATag, $source);						
					}
				}
			}
		}
		return $source;
	}
	
	public function createPageIds($table) {
		$rows = $this->getTables($table);
		$searchChars = array('ä', 'Ä', 'ö', 'Ö', 'ü', 'Ü', 'ß', ' ', '&');
		$replaceChars = array('ae', 'ae', 'oe', 'oe', 'ue', 'ue', 'ss', '_', '_');
		foreach($rows as $row){
			$action = strtolower($row['title']);		
			$action = str_replace($searchChars, $replaceChars, $action); 
			$this->pageActions[$row['uid']] = $action;
		} 
		//print_r($this->pageActions); die();
	}
	
	public function cleanString($content){
		$content = str_replace('<p>', '', $content); 
		$content = str_replace('</p>', '\\n', $content); 
		$content = str_replace('&nbsp;', ' ', $content); 		
		//remove last \n if it is on the end of $content
		if((strlen($content) - strrpos($content, '\\n')) == 2){
			$content = substr($content, 0, -2);
		}
		return addslashes($content);
	}
	
	public function setLevel($uid, $level){
		foreach($this->pageRows as $key => $row){
			if($row['pid'] == $uid){
				$this->roots[] = $row['pid'];
				$this->pageRows[$key]['level'] = $level;
				$this->setLevel($row['uid'], ($level + 1));
			}
		}
	}
	
	public function addQuotes($rows){
		foreach($rows as $key1 => $row){
			foreach($row as $key2 => $value){
				$rows[$key1][$key2] = '\'' . $value . '\'';
			}
		}
		return $rows;
	}
		
	public function getTables($table){
		$rows = array();
		//$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;	
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			$table['fields'],
			$table['table'],
			$table['where']
		);
		//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)){				
			$rows[] = $row;
		}
		return $rows;
	}
	
	public function prepareTables(){ 
		unlink('mysqlitedb.sqlite');
		$this->database = new \SQLite3('mysqlitedb.sqlite');	
		$sql = "
			BEGIN TRANSACTION;
			CREATE TABLE Z_PRIMARYKEY (Z_ENT INTEGER PRIMARY KEY, Z_NAME VARCHAR, Z_SUPER INTEGER, Z_MAX INTEGER);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (1,'Banner',0,3);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (2,'Beacon',0,10);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (3,'Company',0,1);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (4,'Config',0,1);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (5,'Event',0,1);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (6,'Favorite',0,18);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (7,'Image',0,3);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (8,'Menu',0,0);
			INSERT INTO Z_PRIMARYKEY (Z_ENT,Z_NAME,Z_SUPER,Z_MAX) VALUES (9,'Page',0,2);
			CREATE TABLE Z_MODELCACHE (Z_CONTENT BLOB);
			CREATE TABLE Z_METADATA (Z_VERSION INTEGER PRIMARY KEY, Z_UUID VARCHAR(255), Z_PLIST BLOB);
			CREATE TABLE Z_7PAGE ( Z_7IMAGE2 INTEGER, Z_9PAGE INTEGER, PRIMARY KEY (Z_7IMAGE2, Z_9PAGE) );
			CREATE TABLE Z_5IMAGE ( Z_5EVENT1 INTEGER, Z_7IMAGE1 INTEGER, PRIMARY KEY (Z_5EVENT1, Z_7IMAGE1) );			
			CREATE TABLE Z_3IMAGE ( Z_3COMPANY1 INTEGER, Z_7IMAGE INTEGER, PRIMARY KEY (Z_3COMPANY1, Z_7IMAGE) );			
			CREATE TABLE Z_3EVENT ( Z_3COMPANY INTEGER, Z_5EVENT INTEGER, PRIMARY KEY (Z_3COMPANY, Z_5EVENT) );			
			CREATE TABLE ZPAGE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZPAGEID VARCHAR, ZTEXT VARCHAR, ZTITLE VARCHAR, ZTYPE VARCHAR );
			CREATE TABLE ZMENU ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZLEVEL INTEGER, ZORDER INTEGER, ZPARENT INTEGER, ZACTIONID VARCHAR, ZMENUID VARCHAR, ZTITLE VARCHAR );
			CREATE TABLE ZIMAGE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZNAME VARCHAR, ZURL VARCHAR );
			CREATE TABLE ZFAVORITE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZOBJECTID VARCHAR, ZTYPE VARCHAR );
			CREATE TABLE ZEVENT ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZEVENTID VARCHAR, ZLOCATION VARCHAR, ZSPEAKERFUNCTION VARCHAR, ZSPEAKERPOSITION VARCHAR, ZSPEAKERS VARCHAR, ZTEXT VARCHAR, ZTIME VARCHAR, ZTITLE VARCHAR );
			CREATE TABLE ZCONFIG ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZNUMBER_OF_BEACONS_REQUIRED INTEGER );
			CREATE TABLE ZCOMPANY ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZCOMPANYID VARCHAR, ZLOCATION VARCHAR, ZNAME VARCHAR );
			CREATE TABLE ZBEACON ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZVISITED INTEGER, ZBEACONID VARCHAR, ZLINK VARCHAR, ZMAJOR VARCHAR, ZMINOR VARCHAR, ZNAME VARCHAR, ZTEXT VARCHAR, ZUUID VARCHAR );
			CREATE TABLE ZBANNER ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZIMAGE INTEGER, ZEND TIMESTAMP, ZSTART TIMESTAMP, ZBANNERID VARCHAR, ZPAGEID VARCHAR, ZPOSITION VARCHAR, ZSIZE VARCHAR, ZTYPE VARCHAR, ZURL VARCHAR );
			CREATE INDEX Z_7PAGE_Z_9PAGE_INDEX ON Z_7PAGE (Z_9PAGE, Z_7IMAGE2);
			CREATE INDEX Z_5IMAGE_Z_7IMAGE1_INDEX ON Z_5IMAGE (Z_7IMAGE1, Z_5EVENT1);
			CREATE INDEX Z_3IMAGE_Z_7IMAGE_INDEX ON Z_3IMAGE (Z_7IMAGE, Z_3COMPANY1);
			CREATE INDEX Z_3EVENT_Z_5EVENT_INDEX ON Z_3EVENT (Z_5EVENT, Z_3COMPANY);
			CREATE INDEX ZMENU_ZPARENT_INDEX ON ZMENU (ZPARENT);
			CREATE INDEX ZBANNER_ZIMAGE_INDEX ON ZBANNER (ZIMAGE);
			COMMIT;
		";			
		$this->database->query($sql);
	}
}

/*
0.82944488525391 tstamp
0.1576681137085 prepare
9.9635670185089 menu
1.7345499992371 image
0.32925915718079 banner
3.3726789951324 company
1.1490700244904 event
2.7136430740356 beacon
*/
