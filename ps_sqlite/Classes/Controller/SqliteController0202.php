<?php
namespace Peaksourcing\PsSqlite\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * SqliteController
 */
class SqliteController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * sqliteRepository
	 *
	 * @var \Peaksourcing\PsSqlite\Domain\Repository\SqliteRepository
	 * @inject
	 */
	protected $sqliteRepository = NULL;
	
	public $pageActions = array();

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		//$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;	
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'tx_pssqlite_domain_model_sqlite',
			''
		);
		//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;

		if ($GLOBALS['TYPO3_DB']->sql_num_rows($result)) {     
			$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
			$this->sqliteHash = $row['hash'];
			$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
				'*',
				'pages a, 
				tx_pstables_domain_model_image b, 
				tx_pstables_domain_model_banner c, 
				tx_pstables_domain_model_company d, 
				tx_pstables_domain_model_event e'
				#comment this for test - begin
				
				,'not a.deleted AND not a.hidden
				AND not b.deleted AND not b.hidden
				AND not c.deleted AND not c.hidden
				AND not d.deleted AND not d.hidden
				AND not e.deleted AND not e.hidden
				AND (a.tstamp > ' . $row['hash'] . '
				OR b.tstamp > ' . $row['hash'] . ' 
				OR c.tstamp > ' . $row['hash'] . '
				OR d.tstamp > ' . $row['hash'] . '
				OR e.tstamp > ' . $row['hash'] . ')'
				
				#comment this for test - end
			);
			if ($GLOBALS['TYPO3_DB']->sql_num_rows($result)) {  
				if($this->updateSqlite()){	
					$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
						'tx_pssqlite_domain_model_sqlite',
						'uid=' . $row['uid'],
						array(
							'hash' => $this->sqliteHash,
							'manifest' => $this->manifest,
						)
					);
				}
			} else {
				$this->manifest = $row['manifest'];
			}
		} else {			
			if($this->updateSqlite()){
				$result = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
					'tx_pssqlite_domain_model_sqlite',
					array(
						'hash' => $this->sqliteHash,
						'manifest' => $this->manifest,
					)
				);
			}
		}
		  
		$this->view->assign('response', $this->manifest);
		  
		  
						############################
						#### TEST ##################
						############################
						
						$this->database = new \SQLite3('mysqlitedb.sqlite');	
						$sql = 'SELECT * FROM ZEVENT';	
						//$sql = 'SHOW TABLES';	
						$rows = $this->database->query($sql);
						$results = array();
						if($rows) {
							// make assoc array
							while ($row = $rows->fetchArray(SQLITE3_ASSOC)) {
								$results[] = $row;
								//echo $row['ZACTIONID'] . chr(10);
							}
						}
						//print_r($results);	
						
						##############################
						#### TEST ####################
						##############################		  
	}
	
	public function updateSqlite(){	
		//mb_internal_encoding('UTF-8');
		//mb_convert_encoding($row['title'], 'UTF-8')
		//header('Content-Type: application/json; charset=utf-8');
		$this->sqliteHash = time();	
		$this->prepareTables();
		$sql = '';
		
		$tables = array(
			
			0 => array(
				'table' => 'pages',
				'fields' => 'uid, pid, title, sorting, ctvcontent, ctvcontent2, ctvtype, ctvimage',
				'where' => 'not hidden AND not deleted AND doktype=1'
			),
			
			1 => array(
				'table' => 'tx_pstables_domain_model_image a, sys_file_reference b, sys_file c',
				'fields' => 'a.uid, a.pid, a.tstamp, a.link, c.identifier as image',
				'where' => 'not a.hidden AND not a.deleted AND not b.hidden AND not b.deleted AND b.tablenames = \'tx_pstables_domain_model_image\' AND b.uid_foreign = a.uid AND b.uid_local = c.uid',
			),
			
			2 => array(
				'table' => 'tx_pstables_domain_model_banner',
				'fields' => '*',
				'where' => 'not hidden AND not deleted'
			),
			
			3 => array(
				'table' => 'tx_pstables_domain_model_company',
				'fields' => '*',
				'where' => 'not hidden AND not deleted'
			),
			
			4 => array(
				'table' => 'tx_pstables_domain_model_event',
				'fields' => '*',
				'where' => 'not hidden AND not deleted'
			),
		);
		
		$sql .= $this->getZMENUandZPAGEandZ_6PAGE($tables[0]);
		$sql .= $this->getZIMAGE($tables[1]);		
		$sql .= $this->getZBANNER($tables[2]);		
		$sql .= $this->getZCOMPANYandZ_3IMAGE($tables[3]);		
		$sql .= $this->getZEVENTandZ_4IMAGEandZ_3EVENT($tables[4]);		
		
		//print_r($sql);
		$this->database->exec($sql);
		
		
		$this->manifest = array(
			'manifest' => array(
				0 => array(
					'file_name' => 'mysqlitedb.sqlite',
					'url' => 'http://' . $_SERVER['SERVER_NAME'] . '/mysqlitedb.sqlite',
					'hash' => '' . $this->sqliteHash . ''
				),
			),
		);
		$this->manifest['manifest'] = array_merge($this->manifest['manifest'], $this->manifestImg);
		$this->manifest = json_encode($this->manifest, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
		
		return TRUE;
	}
	
	public function getZEVENTandZ_4IMAGEandZ_3EVENT($table){
		$rows = $this->getTables($table);
		foreach($rows as $row){
			#ZEVENT
			//(1,4,1,'testEvent','room #42',NULL,'Speaker1, Speaker 2, Speaker 3','some loerm ipsum ore lorem ipsum and some more','11:00 - 12:00','Some event');
			$sql .= 'INSERT INTO ZEVENT (Z_PK,Z_ENT,Z_OPT,ZEVENTID,ZLOCATION,ZSPEAKERPOSITION,ZSPEAKERS,ZTEXT,ZTIME,ZTITLE)
			VALUES (
			' . $row['uid'] . ',
			4,
			1,
			\'event_' . $row['uid'] . '\',
			\'' . $row['location'] . '\',
			\'' . $row['speakerposition'] . '\',
			\'' . $row['speakers'] . '\',
			\'' . $row['content'] . '\',
			\'' . date('d.m.Y', $row['start']) . ' ' . date('H:i', $row['start']) . ' - ' . date('H:i', $row['end']) . '\',
			\'' . $row['title'] . '\');' . chr(10);		
			
			#Z_4IMAGE
			if($row['image']){
				$sql .= 'INSERT INTO Z_4IMAGE (Z_4EVENT1,Z_6IMAGE1)
				VALUES (
				' . $row['uid'] . ',
				' . $row['image'] . ');' . chr(10);	
			}			
			
			#Z_3EVENT
			$sql .= 'INSERT INTO Z_3EVENT (Z_3COMPANY,Z_4EVENT)
			VALUES (
			' . $row['company'] . ',
			' . $row['uid'] . ');' . chr(10);	
		}	
		//print_r($sql);die();
		return $sql;
	}
	
	public function getZCOMPANYandZ_3IMAGE($table){
		$rows = $this->getTables($table);
		foreach($rows as $row){
			#ZCOMPANY
			//VALUES (1,3,1,'testCompany','11, 12, 13, 14','Some company name');
			$sql .= 'INSERT INTO ZCOMPANY (Z_PK,Z_ENT,Z_OPT,ZCOMPANYID,ZLOCATION,ZNAME)	
			VALUES (' . $row['uid'] . ',
			3,
			1,
			\'company_' . $row['uid'] . '\',
			\'' . $row['location'] . '\',
			\'' . $row['title'] . '\');' . chr(10);	

			#Z_3IMAGE
			if($row['image']){
				$sql .= 'INSERT INTO Z_3IMAGE (Z_3COMPANY1,Z_6IMAGE)			
				VALUES (
				' . $row['uid'] . ',
				' . $row['image'] . ');' . chr(10);	
			}
		}	
		//print_r($sql);die();
		return $sql;
	}

	public function getZBANNER($table){
		$rows = $this->getTables($table);
		foreach($rows as $row){
			$sql .= 'INSERT INTO ZBANNER (Z_PK,Z_ENT,Z_OPT,ZIMAGE,ZBANNERID,ZPAGEID,ZPOSITION,ZSIZE,ZTYPE,ZURL)	
			VALUES (' . $row['uid'] . ',
			1,
			1,
			' . $row['image'] . ',
			\'banner_' . $row['uid'] . '\',
			\'' . $this->pageActions[$row['page']] . '\',
			' . $row['position'] . ',
			\'' . $row['size'] . '\',
			\'' . $row['btype'] . '\',
			\'' . $row['url'] . '\');' . chr(10);		
		}	
		//print_r($sql);die();
		return $sql;
	}	

	public function getZIMAGE($table){
		//ZIMAGE		
		$rows = $this->getTables($table);
		foreach($rows as $key => $row){
			$row['image'] = 'fileadmin' . $row['image'];
			$file_name = substr($row['image'], (strrpos($row['image'], '/') + 1));
			if(!in_array($file_name, $this->file_names)){
				$this->file_names[] = $file_name;
				$this->manifestImg[] = array(
					'file_name' => $file_name,
					'url' => 'http://' . $_SERVER['SERVER_NAME'] . '/' . $row['image'],
					'hash' => '' . $row['tstamp'] . ''
				);				
			}
			$rows[$key]['file_name'] = $file_name;
			$rows[$key]['link'] = intval($row['link']) ? 'index.php?id=' . $row['link'] : $row['link'];
		}
		$rows = $this->addQuotes($rows);
		foreach($rows as $row){
			$sql .= 'INSERT INTO ZIMAGE (Z_PK,Z_ENT,Z_OPT,ZNAME,ZURL) 
			VALUES (' . $row['uid'] . ',
			6,
			1,
			' . $row['file_name'] . ',
			' . $row['link'] . ');' . chr(10);		
		}	

		return $sql;
	}

	public function getZMENUandZPAGEandZ_6PAGE($table) {
		//PAGES
		$rows = $this->getTables($table);
		$this->pageRows = $rows;
		$this->setLevel(1, 0);
		$rows = $this->pageRows;
		//$rows = $this->addQuotes($this->pageRows);
		$searchChars = array('ä', 'Ä', 'ö', 'Ö', 'ü', 'Ü', 'ß', ' ');
		$replaceChars = array('ae', 'ae', 'oe', 'oe', 'ue', 'ue', 'ss', '_');
		foreach($rows as $row){
			if($row['pid'] != 0){
				$action = strtolower($row['title']);		
				$action = str_replace($searchChars, $replaceChars, $action); 
				$this->pageActions[$row['uid']] = $action;
				
				#ZMENU
				if($row['uid'] != 36){			
					$sql .= 'INSERT INTO ZMENU (Z_PK,Z_ENT,Z_OPT,ZLEVEL,ZORDER,ZPARENT,ZACTIONID,ZMENUID,ZTITLE) 
					VALUES (
					' . $row['uid'] . ',
					7,
					1,
					\'' . ($row['level'] ? $row['level'] : '0') . '\',
					\'' . $row['sorting'] . '\',
					\'' . ($row['level'] ? $row['pid'] : 'NULL') . '\',
					\'' . $action . '\',
					\'' . $action . '\',
					\'' . $row['title'] . '\');' . chr(10);	
				}
				
				#ZPAGE			
				if($row['ctvtype'] == ''){
					$row['ctvtype'] = 'page';
				}
				if($row['uid'] == 14 || $row['uid'] == 36){					
					$content = '{"first_section" : "' . $row['ctvcontent'] . '", "second_section" : "' . $row['ctvcontent2'] . '"}';
					$content = str_replace('<p>', '', $content); 
					$content = str_replace('</p>', '\\n', $content); 
					$content = str_replace('&nbsp;', ' ', $content); 
				} else {
					$content = $row['ctvcontent'];
				}
				$sql .= 'INSERT INTO ZPAGE (Z_PK,Z_ENT,Z_OPT,ZPAGEID,ZTEXT,ZTITLE,ZTYPE)
				VALUES (
				' . $row['uid'] . ',
				8,
				1,
				\'' . $action . '\',
				\'' . $content . '\',
				\'' . $row['title'] . '\',
				\'' . $row['ctvtype'] . '\');' . chr(10);	
				
				#Z_6PAGE	
				if($row['ctvimage'] != 0){					
					$sql .= 'INSERT INTO Z_6PAGE (Z_6IMAGE2,Z_8PAGE)
					VALUES (
					' . $row['ctvimage'] . ',
					' . $row['uid'] . ');' . chr(10);	
				}				
			}			
		}
		
		/*
		foreach($rows as $row){
			if($row['pid'] != '\'0\''){
				$action = strtolower($row['title']);		
				$action = str_replace($searchChars, $replaceChars, $action); 
				$this->pageActions[$row['uid']] = $action;
				
				#ZMENU
				$sql .= 'INSERT INTO ZMENU (Z_PK,Z_ENT,Z_OPT,ZLEVEL,ZORDER,ZPARENT,ZACTIONID,ZMENUID,ZTITLE) 
				VALUES (
				' . $row['uid'] . ',
				7,
				1,
				' . ($row['level'] ? $row['level'] : '0') . ',
				' . $row['sorting'] . ',
				' . ($row['level'] ? $row['pid'] : 'NULL') . ',
				' . $action . ',
				' . $action . ',
				' . $row['title'] . ');' . chr(10);	

				#ZPAGE			
				if($row['ctvtype'] == '\'\''){
					$row['ctvtype'] = '\'page\'';
				}
				if($row['uid'] == '\'14\'' || $row['uid'] == '\'35\''){
					$content = '\'{"first_section" : "assssss", "second_section" : "aaaaaa"}\'';
				} else {
					$content = $row['ctvcontent'];
				}
				$sql .= 'INSERT INTO ZPAGE (Z_PK,Z_ENT,Z_OPT,ZPAGEID,ZTEXT,ZTITLE,ZTYPE)
				VALUES (
				' . $row['uid'] . ',
				8,
				1,
				' . $action . ',
				' . $row['ctvcontent'] . ',
				' . $row['title'] . ',
				' . $row['ctvtype'] . ');' . chr(10);	
				
				#Z_6PAGE	
				if($row['ctvimage'] != '\'0\''){					
					$sql .= 'INSERT INTO Z_6PAGE (Z_6IMAGE2,Z_8PAGE)
					VALUES (
					' . $row['ctvimage'] . ',
					' . $row['uid'] . ');' . chr(10);	
				}
			}			
		}
		*/
		
		
		//print_r($sql);die();
		return $sql;
	}
	
	public function setLevel($uid, $level){
		foreach($this->pageRows as $key => $row){
			if($row['pid'] == $uid){
				$this->pageRows[$key]['level'] = $level;
				$this->setLevel($row['uid'], ($level + 1));
			}
		}
	}
	
	public function addQuotes($rows){
		foreach($rows as $key1 => $row){
			foreach($row as $key2 => $value){
				$rows[$key1][$key2] = '\'' . $value . '\'';
			}
		}
		return $rows;
	}
		
	public function getTables($table){
		$rows = array();
		//$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;	
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			$table['fields'],
			$table['table'],
			$table['where']
		);
		//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)){				
			$rows[] = $row;
		}
		return $rows;
	}
	
	public function prepareTables(){
		unlink('mysqlitedb.sqlite');
		$this->database = new \SQLite3('mysqlitedb.sqlite');	 		
		$sql = "
			BEGIN TRANSACTION;
			CREATE TABLE Z_PRIMARYKEY (Z_ENT INTEGER PRIMARY KEY, Z_NAME VARCHAR, Z_SUPER INTEGER, Z_MAX INTEGER);
			CREATE TABLE Z_MODELCACHE (Z_CONTENT BLOB);
			CREATE TABLE Z_METADATA (Z_VERSION INTEGER PRIMARY KEY, Z_UUID VARCHAR(255), Z_PLIST BLOB);
			CREATE TABLE Z_6PAGE ( Z_6IMAGE2 INTEGER, Z_8PAGE INTEGER, PRIMARY KEY (Z_6IMAGE2, Z_8PAGE) );
			CREATE TABLE Z_4IMAGE ( Z_4EVENT1 INTEGER, Z_6IMAGE1 INTEGER, PRIMARY KEY (Z_4EVENT1, Z_6IMAGE1) );
			CREATE TABLE Z_3IMAGE ( Z_3COMPANY1 INTEGER, Z_6IMAGE INTEGER, PRIMARY KEY (Z_3COMPANY1, Z_6IMAGE) );
			CREATE TABLE Z_3EVENT ( Z_3COMPANY INTEGER, Z_4EVENT INTEGER, PRIMARY KEY (Z_3COMPANY, Z_4EVENT) );
			CREATE TABLE ZPAGE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZPAGEID VARCHAR, ZTEXT VARCHAR, ZTITLE VARCHAR, ZTYPE VARCHAR );
			CREATE TABLE ZMENU ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZLEVEL INTEGER, ZORDER INTEGER, ZPARENT INTEGER, ZACTIONID VARCHAR, ZMENUID VARCHAR, ZTITLE VARCHAR );
			CREATE TABLE ZIMAGE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZNAME VARCHAR, ZURL VARCHAR );
			CREATE TABLE ZFAVORITE ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZOBJECTID VARCHAR, ZTYPE VARCHAR );
			CREATE TABLE ZEVENT ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZEVENTID VARCHAR, ZLOCATION VARCHAR, ZSPEAKERPOSITION VARCHAR, ZSPEAKERS VARCHAR, ZTEXT VARCHAR, ZTIME VARCHAR, ZTITLE VARCHAR );
			CREATE TABLE ZCOMPANY ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZCOMPANYID VARCHAR, ZLOCATION VARCHAR, ZNAME VARCHAR );
			CREATE TABLE ZBEACON ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZBEACONID VARCHAR, ZNAME VARCHAR );
			CREATE TABLE ZBANNER ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZIMAGE INTEGER, ZBANNERID VARCHAR, ZPAGEID VARCHAR, ZPOSITION VARCHAR, ZSIZE VARCHAR, ZTYPE VARCHAR, ZURL VARCHAR );
			CREATE INDEX Z_6PAGE_Z_8PAGE_INDEX ON Z_6PAGE (Z_8PAGE, Z_6IMAGE2);
			CREATE INDEX Z_4IMAGE_Z_6IMAGE1_INDEX ON Z_4IMAGE (Z_6IMAGE1, Z_4EVENT1);
			CREATE INDEX Z_3IMAGE_Z_6IMAGE_INDEX ON Z_3IMAGE (Z_6IMAGE, Z_3COMPANY1);
			CREATE INDEX Z_3EVENT_Z_4EVENT_INDEX ON Z_3EVENT (Z_4EVENT, Z_3COMPANY);
			CREATE INDEX ZMENU_ZPARENT_INDEX ON ZMENU (ZPARENT);
			CREATE INDEX ZBANNER_ZIMAGE_INDEX ON ZBANNER (ZIMAGE);
			COMMIT;
		";			
		$this->database->query($sql);
	}
}