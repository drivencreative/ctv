<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Peaksourcing.' . $_EXTKEY,
	'Sqlite',
	'Sqlite'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_sqlite';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_sqlite.xml');

/*
if (TYPO3_MODE === 'BE') {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        $_EXTKEY,
        'web',          // Main area
        'mod1',         // Name of the module
        '',             // Position of the module
        array(          // Allowed controller action combinations
            'Module' => 'list',
        ),
        array(          // Additional configuration
            'access'    => 'user,group',
            //'icon'      => 'EXT:blog_example/ext_icon.gif',
            'labels'    => 'Edit Page',
        )
    );
}
*/

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Sqlite');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pssqlite_domain_model_sqlite', 'EXT:ps_sqlite/Resources/Private/Language/locallang_csh_tx_pssqlite_domain_model_sqlite.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pssqlite_domain_model_sqlite');
