<?php
namespace PS\PsNotify\Hooks;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Torben Hansen <derhansen@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Hooks for DataHandler
 */
class DataHandlerHooks {	

	public function processDatamap_postProcessFieldArray($status, $table, $id, &$fieldArray, &$reference){
		/*
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			$table,
			'',
			'',
			'uid desc',
			2
		);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)){			
			print_r($row); 
		}
		die();		
		*/
	}
	
	/**
	 * Checks if the fields defined in $checkFields are set in the data-array of pi_flexform. If a field is
	 * present and contains an empty value, the field is unset.
	 *
	 * Structure of the checkFields array:
	 *
	 * array('sheet' => array('field1', 'field2'));
	 *
	 * @param string $status
	 * @param string $table
	 * @param string $id
	 * @param array $fieldArray
	 * @param \TYPO3\CMS\Core\DataHandling\DataHandler $reference
	 *
	 * @return void
	 */	 
	public function processDatamap_afterDatabaseOperations($status, $table, $id, &$fieldArray, &$reference){
		if($table == 'tx_psnotify_domain_model_push'){		
			//print_r($id);die();
			//if INSERT
			if($fieldArray['content']){
				$message = $fieldArray['content'];
			} else {
				//if UPDATE
				$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
					'*',
					$table,
					'uid=' . $id
				);

				$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
				$message = $row['content'];
			}
			
			$tokens = $this->getTokens();
			
			//print_r($androidRegistrationIds);die();
			//$androidRegistrationIds = array('cWCHpRjCDi8:APA91bEsF8chCXVQqPHKScS6fPMzXbi6lZUO1Roahm_Bee6xvIvidzHSMthsLUzlpgBP16XZZofn2-SDLtnQOV8dr4MFLYd9vmpYaAw1eUDFvkEPYY7iGUiJ9hAlts_ZI-EAjK5L_JEn');
			$this->sendAndroidNotifications($tokens['android'], $message);
			//$iosRegistrationIds = array('cWCHpRjCDi8:APA91bEsF8chCXVQqPHKScS6fPMzXbi6lZUO1Roahm_Bee6xvIvidzHSMthsLUzlpgBP16XZZofn2-SDLtnQOV8dr4MFLYd9vmpYaAw1eUDFvkEPYY7iGUiJ9hAlts_ZI-EAjK5L_JEn');
			$this->sendIOSNotifications($tokens['ios'], $message);			
		}
	}	
	
	public function getTokens(){
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'not deleted'
		);
		$androidRegistrationIds = array();
		$iosRegistrationIds = array(); 
		
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)){				
			foreach(json_decode($row['tx_psfeuser_api_token']) as $obj){	
				if($obj->push && strpos(strtolower($obj->device),'dpi') !== FALSE){	
					//echo $obj->device . chr(10);
					$androidRegistrationIds[] = $obj->push;
				}
				if($obj->push && strpos(strtolower($obj->device),'iphone') !== FALSE){	
					//echo $obj->device . chr(10);
					$iosRegistrationIds[] = $obj->push;
				}
			}	
		}	
		
		return(array(
			'android' => $androidRegistrationIds,
			'ios' => $iosRegistrationIds,
		));
	}

	public function sendAndroidNotifications($registrationIds, $message) {
		$url = 'https://gcm-http.googleapis.com/gcm/send';

		$fields = array(
			'registration_ids' => $registrationIds,
			'data' => array( 'message' => $message )
		);
		
		$headers = array(
			//stari marko 'Authorization: key= AIzaSyCb4889qzaGZR_1YuqjrPXLH9IjPBXUGGo',
			//novi marko 'Authorization: key= AIzaSyBOFlD2gKyOUX8wsi3EOaFoBi7PjTe84xI',
			//miladin 'Authorization: key= AIzaSyDJgHc3UMINelKPTRJTQ-ancDHi5MEdqDw',
			'Authorization: key= AIzaSyBOFlD2gKyOUX8wsi3EOaFoBi7PjTe84xI',
			'Content-Type: application/json'
		);
		
		$headers = array(
			'Authorization: key= AIzaSyBOFlD2gKyOUX8wsi3EOaFoBi7PjTe84xI',
			'Content-Type: application/json'
		);
		// Open connection
		$ch = curl_init();
	
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
	
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	
		// Execute post
		$result = curl_exec($ch);

		$this->writeToLog($result);
		
		// Close connection
		curl_close($ch);
	}
	
	/**
	 * Send push notifications to iOS devices
	 *
	 * @param array $registrationIds
	 * @param string $message
	 *
	 */
	public function sendIOSNotifications($registrationIds, $message) {
		$delivered = false;
		foreach($registrationIds as $deviceToken) {
			//stream_context_set_option($ctx, 'ssl', 'local_cert', '/srv/www/ioe/public_html/typo3conf/ext/ilc_mobile/Classes/Domain/dev.pem');
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/srv/www/ctv/public_html/typo3conf/ext/ps_notify/Resources/Public/distro.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', 'also');
			
			// Open a connection to the APNS server
			$fp = stream_socket_client(
				'ssl://gateway.push.apple.com:2195', $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			
			if (!$fp)
				exit("Failed to connect: $err - $errstr" . PHP_EOL);
			
			// Create the payload body
			$body['aps'] = array(
				'alert' => array(
					'body' => $message
				)
			);
			
			// Encode the payload as JSON
			$payload = json_encode($body);
			
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = '{"aps":{"alert": {"body" : "' . $message . '"}}}';
			//echo $msg; die();
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			
			if (!$result) {
				echo 'Message not delivered' . PHP_EOL;
			} else {
				$delivered = true;
			}			
			#else
			#	echo 'Message successfully delivered' . PHP_EOL;
			
			// Close the connection to the server
			fclose($fp);			
		}
		if($delivered){			
			$this->writeToLog('delivered');
		}
	}
	
	public function writeToLog($result){
		$myfile = fopen('../../public_html/logs/notifications.txt', 'a') or die('Unable to open file!');
		$txt = 'response: ' . $result . '| sent: ' . date('d:m:Y H:i:s');  
		fwrite($myfile, $txt . PHP_EOL);
		fclose($myfile);
	}
}