<?php
namespace PS\PsNotify\Command;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * PushCommandController
 */
class PushCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController {
	
	/**
	 * @var Tx_Extbase_Configuration_ConfigurationManagerInterface
	 * @inject
	 */
	protected $configurationManager;	
	
	public function pushCommand() {		
		//$this->settings = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'PsNotify', 'Notify');
		//print_r($this->settings);
		$pushObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('PS\\PsNotify\\Hooks\\DataHandlerHooks');
		
		$resultConf = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'tx_pstables_domain_model_conf',
			'not hidden and not deleted'
		);
		
		$rowConf = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resultConf);
		
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'tx_pstables_domain_model_event',
			'not hidden and not deleted'
		);

		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)){
			if($row['start'] < (time() + $rowConf['timebeforeevent']) && $row['start'] > time() && !$row['notified']){		
				$message = $rowConf['notifytext'];
				$message = str_replace('###name###', $row['title'], $message);
				$message = str_replace('###time###', date('d.m.Y H:i', $row['start']), $message);
				$message = str_replace('###room###', $row['location'], $message);			
				//$message = $row['title'] . ' um ' . date('H:i', $row['start']) . ' Uhr';
				
				$tokens = $pushObj->getTokens($row['uid']);
				$pushObj->sendAndroidNotifications($tokens['android'], $message);
				$pushObj->sendIOSNotifications($tokens['ios'], $message);	
				$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
					'tx_pstables_domain_model_event',
					'uid=' . $row['uid'],
					array( 
						'notified' => 1, 
					)
				);
			}
		}		
	}
	
}