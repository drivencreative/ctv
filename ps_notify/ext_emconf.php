<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "ps_notify"
 *
 * Auto generated by Extension Builder 2016-02-19
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Push notifications',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Miladin Bojic',
	'author_email' => 'miladin.bojic@googlemail.com',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2.0-6.2.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);