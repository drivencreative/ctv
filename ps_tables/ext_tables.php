<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'CTV Tables');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pstables_domain_model_image', 'EXT:ps_tables/Resources/Private/Language/locallang_csh_tx_pstables_domain_model_image.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pstables_domain_model_image');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pstables_domain_model_event', 'EXT:ps_tables/Resources/Private/Language/locallang_csh_tx_pstables_domain_model_event.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pstables_domain_model_event');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pstables_domain_model_company', 'EXT:ps_tables/Resources/Private/Language/locallang_csh_tx_pstables_domain_model_company.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pstables_domain_model_company');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pstables_domain_model_banner', 'EXT:ps_tables/Resources/Private/Language/locallang_csh_tx_pstables_domain_model_banner.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pstables_domain_model_banner');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pstables_domain_model_beacon', 'EXT:ps_tables/Resources/Private/Language/locallang_csh_tx_pstables_domain_model_beacon.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pstables_domain_model_beacon');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pstables_domain_model_conf', 'EXT:ps_tables/Resources/Private/Language/locallang_csh_tx_pstables_domain_model_conf.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pstables_domain_model_conf');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pstables_domain_model_companycategory', 'EXT:ps_tables/Resources/Private/Language/locallang_csh_tx_pstables_domain_model_companycategory.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pstables_domain_model_companycategory');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pstables_domain_model_teaser', 'EXT:ps_tables/Resources/Private/Language/locallang_csh_tx_pstables_domain_model_teaser.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pstables_domain_model_teaser');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
	$_EXTKEY,
	'tx_pstables_domain_model_companycategory'
);

