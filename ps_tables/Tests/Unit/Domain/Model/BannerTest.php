<?php

namespace Ps\PsTables\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Ps\PsTables\Domain\Model\Banner.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Miladin Bojic <miladin.bojic@gmail.com>
 */
class BannerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \Ps\PsTables\Domain\Model\Banner
	 */
	protected $subject = NULL;

	public function setUp() {
		$this->subject = new \Ps\PsTables\Domain\Model\Banner();
	}

	public function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getSizeReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getSize()
		);
	}

	/**
	 * @test
	 */
	public function setSizeForStringSetsSize() {
		$this->subject->setSize('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'size',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getBtypeReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getBtype()
		);
	}

	/**
	 * @test
	 */
	public function setBtypeForStringSetsBtype() {
		$this->subject->setBtype('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'btype',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getUrlReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getUrl()
		);
	}

	/**
	 * @test
	 */
	public function setUrlForStringSetsUrl() {
		$this->subject->setUrl('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'url',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPositionReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getPosition()
		);
	}

	/**
	 * @test
	 */
	public function setPositionForStringSetsPosition() {
		$this->subject->setPosition('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'position',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPageReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getPage()
		);
	}

	/**
	 * @test
	 */
	public function setPageForStringSetsPage() {
		$this->subject->setPage('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'page',
			$this->subject
		);
	}
}
