<?php
namespace Ps\PsTables\Domain\Repository;


    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Teasers
 */
class ImageRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * example for the entire repository
     */
    public function initializeObject()
    {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');

        /**
         * ignore storage PID initially
         **/
        $querySettings->setRespectStoragePage(false);

        /**
         * set settings
         */
        $this->setDefaultQuerySettings($querySettings);
    }


    /**
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllImages(){

        $Query = $this->createQuery();

        $Query->statement(
            'SELECT f.name as image,c.uid 
              FROM tx_pstables_domain_model_image as c 
		      INNER JOIN sys_file_reference as fr ON fr.uid_foreign=c.uid 
		      INNER JOIN sys_file as f ON f.uid=fr.uid_local 
		      WHERE c.deleted=0 
		      AND c.hidden=0  
		      AND fr.deleted=0 
		      AND fr.hidden=0 
		      AND fr.fieldname="image" 
		      AND fr.tablenames="tx_pstables_domain_model_image"');
        return print_r($Query);

    }
}