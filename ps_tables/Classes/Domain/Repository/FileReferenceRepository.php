<?php
/**
 * Created by PhpStorm.
 * User: Dragan
 * Date: 2/28/2017
 * Time: 12:58 PM
 */

namespace Ps\PsTables\Domain\Repository;


/**
 * The repository for FileReference
 */
class FileReferenceRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'sorting_foreign' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

}