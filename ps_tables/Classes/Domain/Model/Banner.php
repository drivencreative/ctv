<?php
namespace Ps\PsTables\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Banner
 */
class Banner extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * size
	 *
	 * @var string
	 */
	protected $size = '';

	/**
	 * btype
	 *
	 * @var string
	 */
	protected $btype = '';

	/**
	 * url
	 *
	 * @var string
	 */
	protected $url = '';

	/**
	 * position
	 *
	 * @var string
	 */
	protected $position = '';


	/**
	 * positionRecordType
	 *
	 * @var int
	 */
	protected $positionRecordType = 0;

	/**
	 * positionEvent
	 *
	 * @var string
	 */
	protected $positionEvent = '';

	/**
	 * positionCompany
	 *
	 * @var string
	 */
	protected $positionCompany = '';

	/**
	 * page
	 *
	 * @var string
	 */
	protected $page = '';

	/**
	 * Returns the size
	 *
	 * @return string $size
	 */
	public function getSize() {
		return $this->size;
	}

	/**
	 * Sets the size
	 *
	 * @param string $size
	 * @return void
	 */
	public function setSize($size) {
		$this->size = $size;
	}

	/**
	 * Returns the btype
	 *
	 * @return string $btype
	 */
	public function getBtype() {
		return $this->btype;
	}

	/**
	 * Sets the btype
	 *
	 * @param string $btype
	 * @return void
	 */
	public function setBtype($btype) {
		$this->btype = $btype;
	}

	/**
	 * Returns the url
	 *
	 * @return string $url
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Sets the url
	 *
	 * @param string $url
	 * @return void
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * Returns the positionCompany
	 *
	 * @return string $positionCompany
	 */
	public function getPositionCompany() {
		return $this->positionCompany;
	}

	/**
	 * Sets the positionCompany
	 *
	 * @param string $positionCompany
	 * @return void
	 */
	public function setPositionCompany($positionCompany) {
		$this->positionCompany = $positionCompany;
	}

	/**
	 * Returns the positionEvent
	 *
	 * @return string $positionEvent
	 */
	public function getPositionEvent() {
		return $this->positionEvent;
	}

	/**
	 * Sets the positionEvent
	 *
	 * @param string $positionEvent
	 * @return void
	 */
	public function setPositionEvent($positionEvent) {
		$this->positionEvent = $positionEvent;
	}
	
	/**
	 * Returns the position
	 *
	 * @return string $position
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * Sets the position
	 *
	 * @param string $position
	 * @return void
	 */
	public function setPosition($position) {
		$this->position = $position;
	}

	/**
	 * Returns the page
	 *
	 * @return string $page
	 */
	public function getPage() {
		return $this->page;
	}

	/**
	 * Sets the page
	 *
	 * @param string $page
	 * @return void
	 */
	public function setPage($page) {
		$this->page = $page;
	}

	/**
	 * Returns the positionRecordType
	 *
	 * @return int
	 */
	public function getPositionRecordType()
	{
		return $this->positionRecordType;
	}

	/**
	 * Sets the positionRecordType
	 *
	 * @param int $positionRecordType
	 * @return void
	 */
	public function setPositionRecordType($positionRecordType)
	{
		$this->positionRecordType = $positionRecordType;
	}

}