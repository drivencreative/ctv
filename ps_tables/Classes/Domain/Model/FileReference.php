<?php
namespace Ps\PsTables\Domain\Model;

/**
 * A file reference object (File Abstraction Layer)
 */
class FileReference extends \TYPO3\CMS\Extbase\Domain\Model\AbstractFileFolder
{

    /**
     * Uid of the referenced sys_file. Needed for extbase to serialize the
     * reference correctly.
     *
     * @var int
     */
    protected $uidLocal;

    /**
     * Uid of the created User
     *
     * @var int
     */
    protected $cruserId;

    /**
     * Title
     *
     * @var string
     */
    protected $title;

    /**
     * Description
     *
     * @var string
     */
    protected $description;

    /**
     * Alternative
     *
     * @var string
     */
    protected $alternative;

    /**
     * Link
     *
     * @var string
     */
    protected $link;

    /**
     * SortingForeign
     *
     * @var integer
     */
    protected $sortingForeign;

    /**
     * Deleted
     *
     * @var integer
     */
    protected $deleted = 0;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getAlternative()
    {
        return $this->alternative;
    }

    /**
     * @param string $alternative
     */
    public function setAlternative($alternative)
    {
        $this->alternative = $alternative;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getSortingForeign()
    {
        return $this->sortingForeign;
    }

    /**
     * @param int $sortingForeign
     */
    public function setSortingForeign($sortingForeign)
    {
        $this->sortingForeign = $sortingForeign;
    }

    /**
     * @return int
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param int $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return \TYPO3\CMS\Core\Resource\FileReference
     */
    public function getOriginalResource()
    {
        if ($this->originalResource === null) {
            $this->originalResource = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance()->getFileReferenceObject($this->getUid());
        }

        return $this->originalResource;
    }

    /**
     * @param array $originalResource
     * @param string $folder
     */
    public function setOriginalResource($originalResource, $folder = 'user_upload/tx_csvimport')
    {
        $this->originalResource = is_object($originalResource) ? $originalResource : $this->createFalFile($originalResource, $folder);
        if ($this->originalResource) {
            $this->uidLocal = (int)$this->originalResource->getUid();
        }
    }

    /**
     * Upload and Create file reference object (File Abstraction Layer)
     *
     * @param array $file
     * @param string $folder
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected function createFalFile($file, $folder) {
        if(!empty($file['name'])){
            $storage = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Resource\StorageRepository')->findByUid(1);
            return $storage->addFile($file['tmp_name'], $storage->getFolder($folder), $file['name']);
        }
        return null;
    }

}
