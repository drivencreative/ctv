<?php
namespace Ps\PsTables\Domain\Model;

	/***************************************************************
	 *
	 *  Copyright notice
	 *
	 *  (c) 2016 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * Company
 */
class Company extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * location
	 *
	 * @var string
	 */
	protected $location = '';

	/**
	 * category
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Ps\PsTables\Domain\Model\CompanyCategory>
	 * @cascade remove
	 */
	protected $category = NULL;

	/**
	 * image
	 *
	 * @var \Ps\PsTables\Domain\Model\Image
	 */
	protected $image = NULL;
	
	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the location
	 *
	 * @return string $location
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * Sets the location
	 *
	 * @param string $location
	 * @return void
	 */
	public function setLocation($location) {
		$this->location = $location;
	}

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->category = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Adds a CompanyCategory
	 *
	 * @param \Ps\PsTables\Domain\Model\CompanyCategory $category
	 * @return void
	 */
	public function addCategory(\Ps\PsTables\Domain\Model\CompanyCategory $category) {
		$this->category->attach($category);
	}

	/**
	 * Removes a CompanyCategory
	 *
	 * @param \Ps\PsTables\Domain\Model\CompanyCategory $categoryToRemove The CompanyCategory to be removed
	 * @return void
	 */
	public function removeCategory(\Ps\PsTables\Domain\Model\CompanyCategory $categoryToRemove) {
		$this->category->detach($categoryToRemove);
	}

	/**
	 * Returns the category
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Ps\PsTables\Domain\Model\CompanyCategory> $category
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * Sets the category
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Ps\PsTables\Domain\Model\CompanyCategory> $category
	 * @return void
	 */
	public function setCategory(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $category) {
		$this->category = $category;
	}

	/**
	 * Returns the image
	 *
	 * @return \Ps\PsTables\Domain\Model\Image $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \Ps\PsTables\Domain\Model\Image $image
	 * @return void
	 */
	public function setImage(\Ps\PsTables\Domain\Model\Image $image) {
		$this->image = $image;
	}
}