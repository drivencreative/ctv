<?php
namespace Ps\PsTables\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Event
 */
class Event extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * location
	 *
	 * @var string
	 */
	protected $location = '';

	/**
	 * speakerposition
	 *
	 * @var string
	 */
	protected $speakerposition = '';

	/**
	 * speakers
	 *
	 * @var string
	 */
	protected $speakers = '';

	/**
	 * content
	 *
	 * @var string
	 */
	protected $content = '';

	/**
	 * start
	 *
	 * @var \DateTime
	 */
	protected $start = NULL;

	/**
	 * end
	 *
	 * @var \DateTime
	 */
	protected $end = NULL;

	/**
	 * company
	 *
	 * @var \Ps\PsTables\Domain\Model\Company
	 */
	protected $company = NULL;

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the location
	 *
	 * @return string $location
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * Sets the location
	 *
	 * @param string $location
	 * @return void
	 */
	public function setLocation($location) {
		$this->location = $location;
	}

	/**
	 * Returns the speakerposition
	 *
	 * @return string $speakerposition
	 */
	public function getSpeakerposition() {
		return $this->speakerposition;
	}

	/**
	 * Sets the speakerposition
	 *
	 * @param string $speakerposition
	 * @return void
	 */
	public function setSpeakerposition($speakerposition) {
		$this->speakerposition = $speakerposition;
	}

	/**
	 * Returns the speakers
	 *
	 * @return string $speakers
	 */
	public function getSpeakers() {
		return $this->speakers;
	}

	/**
	 * Sets the speakers
	 *
	 * @param string $speakers
	 * @return void
	 */
	public function setSpeakers($speakers) {
		$this->speakers = $speakers;
	}

	/**
	 * Returns the content
	 *
	 * @return string $content
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Sets the content
	 *
	 * @param string $content
	 * @return void
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * Returns the start
	 *
	 * @return \DateTime $start
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Sets the start
	 *
	 * @param \DateTime $start
	 * @return void
	 */
	public function setStart(\DateTime $start) {
		$this->start = $start;
	}

	/**
	 * Returns the end
	 *
	 * @return \DateTime $end
	 */
	public function getEnd() {
		return $this->end;
	}

	/**
	 * Sets the end
	 *
	 * @param \DateTime $end
	 * @return void
	 */
	public function setEnd(\DateTime $end) {
		$this->end = $end;
	}

	/**
	 * Returns the company
	 *
	 * @return \Ps\PsTables\Domain\Model\Company $company
	 */
	public function getCompany() {
		return $this->company;
	}

	/**
	 * Sets the company
	 *
	 * @param \Ps\PsTables\Domain\Model\Company $company
	 * @return void
	 */
	public function setCompany(\Ps\PsTables\Domain\Model\Company $company) {
		$this->company = $company;
	}

}