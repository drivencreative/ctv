<?php
namespace Ps\PsTables\Services;
/**
 * Created by PhpStorm.
 * User: Dragan
 * Date: 2/9/2017
 * Time: 12:12 PM
 */
class UserFuncPageActions
{
    /**
     * @var \Peaksourcing\PsSqlite\Services\SqliteGenerator
     *
     */
    private $data;

    public function fieldForeignTable($params) {
        $this->data = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Peaksourcing\PsSqlite\Services\SqliteGenerator');

        $value = $this->data->createPageIds(
            [
                'table' =>  'pages',
                'fields' => 'uid, pid, title, sorting, ctvcontent, ctvcontent2, ctvtype, ctvimage, ctvbeacons',
                'where' => 'not hidden AND not deleted AND doktype=1'
            ]
        );

        foreach ($value as $val){
            $params['items'][] = array($val, $val);
        }

    }

}