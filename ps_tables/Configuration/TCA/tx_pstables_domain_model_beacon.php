<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'size,btype,url,position,page,title,content,uuid,major,minor,link,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ps_tables') . 'Resources/Public/Icons/tx_pstables_domain_model_beacon.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, gamebeacon, title, content, link, uuid, major, minor',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, gamebeacon, title, content, link, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime, uuid, major, minor'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_pstables_domain_model_beacon',
				'foreign_table_where' => 'AND tx_pstables_domain_model_beacon.pid=###CURRENT_PID### AND tx_pstables_domain_model_beacon.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon.title',
			'config' => array(
				'type' => 'input',
				'size' => 5,
				'eval' => 'trim'
			),
		),
		'uuid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon.uuid',
			'config' => array(
				'type' => 'input',
				'size' => 5,
				'eval' => 'trim'
			),
		),
		'major' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon.major',
			'config' => array(
				'type' => 'input',
				'size' => 5,
				'eval' => 'trim'
			),
		),
		'minor' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon.minor',
			'config' => array(
				'type' => 'input',
				'size' => 5,
				'eval' => 'trim'
			),
		),
		'content' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon.content',
			'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 6
			),
			'defaultExtras' => 'richtext[]'
		),
		'link' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon.link',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
				'wizards'  => array(
					'_PADDING' => 2,
					'link'     => array(
						'type'         => 'popup',
						'title'        => 'Link',
						'icon'         => 'link_popup.gif',
						'script'       => 'browse_links.php?mode=wizard',
						'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1',
						'params'   => array(
							'blindLinkOptions' => 'file,mail,folder,url'
						)
					)
				),
			),
		),
		'gamebeacon' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon.gamebeacon',
			'config' => array(
				'type' => 'check',
			),
		),
	),
);