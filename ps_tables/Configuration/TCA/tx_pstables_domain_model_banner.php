<?php
return array(
    'ctrl' => array(
        'title' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner',
        'label' => 'size',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'type' => 'size',
        'requestUpdate' => 'position_record_type',
        'searchFields' => 'size,btype,url,position,page,company,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ps_tables') . 'Resources/Public/Icons/tx_pstables_domain_model_banner.gif'
    ),
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, company, size, btype, url, position, page, start_time, end_time',
    ),
    'types' => array(
        'bottom' => array('showitem' => 'size, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, url, position, page, start_time, end_time, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
        'full' => array('showitem' => 'size, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, company, image, btype, url, page, start_time, end_time, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
        'inline' => array('showitem' => 'size, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, company, image, btype, url, position_record_type, position_event, position_company, page, start_time, end_time, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
        'interstitial' => array('showitem' => 'size, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, company, image, btype, url, page, start_time, end_time, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime')
    ),
    'palettes' => array(
        '1' => array('showitem' => ''),
    ),
    'columns' => array(

        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_pstables_domain_model_banner',
                'foreign_table_where' => 'AND tx_pstables_domain_model_banner.pid=###CURRENT_PID### AND tx_pstables_domain_model_banner.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),

        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),

        'size' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner.size',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('stickyad', 'bottom'),
                    array('full', 'full'),
                    array('incontent', 'inline'),
                    array('interstitial', 'interstitial'),
                ),
                'default' => 'inline'
            ),
        ),
        'btype' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner.btype',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('every', 'every'),
                    array('daily', 'daily'),
                )
            ),
        ),
        'url' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner.url',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'wizards' => array(
                    '_PADDING' => 2,
                    'link' => array(
                        'type' => 'popup',
                        'title' => 'Link',
                        'icon' => 'link_popup.gif',
                        'script' => 'browse_links.php?mode=wizard',
                        'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1',
                        'params' => array(
                            'blindLinkOptions' => ''
                        )
                    )
                ),
            ),
        ),
        'position_record_type' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner.position_record_type',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('Company', '0'),
                    array('Events', '1')
                )
            ),
            'displayCond' => array(
                'AND' => array(
                    'FIELD:size:=:inline'
                )
            ),
        ),
        'position_event' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner.position_event',
            'displayCond' => array(
                'AND' => array(
                    'FIELD:position_record_type:=:1'
                )
            ),
            'config' => Array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_pstables_domain_model_event',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0',
                'show_thumbs' => '1'
            )
        ),
        'position_company' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner.position_company',
            'displayCond' => array(
                'AND' => array(
                    'FIELD:position_record_type:=:0'
                )
            ),
            'config' => Array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_pstables_domain_model_company',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0',
                'show_thumbs' => '1'
            )
        ),
        'company' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner.company',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'page' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_banner.page',
            'config' => Array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0',
                'show_thumbs' => '1'
            )
        ),
        'image' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_image.image',
            'config' => Array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_pstables_domain_model_image',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0',
                'show_thumbs' => '1'
            )
        ),
        'start_time' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'end_time' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
    ),
);