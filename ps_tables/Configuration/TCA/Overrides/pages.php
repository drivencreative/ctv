<?php
$tempColumns = array(
    'ctvtype' => array(
        'displayCond' => 'FIELD:homepage:=:0',
        'exclude' => 0,
        'label' => 'Type',
        'config' => array(
            'type' => 'select',
            'items' => array(
                array('LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.page', 'page'),
                array('LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.html', 'html'),
                array('LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.htmlink', 'htmlink'),
                array('LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.htmlfile', 'htmlfile'),
                array('LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.profile', 'profile'),
                array('LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.event', 'event'),
                array('LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.company', 'company'),
                array('LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.textonly', 'textonly'),
            )
        ),
    ),
    'ctvcontent' => array(
        'displayCond' => 'FIELD:homepage:=:0',
        'exclude' => 0,
        'label' => 'Content',
        'config' => array(
            'type' => 'text',
            'cols' => 40,
            'rows' => 6
        ),
        'defaultExtras' => 'richtext[]'
    ),
    'ctvcontent2' => array(
        'displayCond' => 'FIELD:homepage:=:0',
        'exclude' => 0,
        'label' => 'Content 2',
        'config' => array(
            'type' => 'text',
            'cols' => 40,
            'rows' => 6
        ),
        'defaultExtras' => 'richtext[]'
    ),
    'ctvimage' => array(
        'displayCond' => 'FIELD:homepage:=:0',
        'exclude' => 0,
        'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_image.image',
        'config' => Array(
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'tx_pstables_domain_model_image',
            'size' => '1',
            'maxitems' => '1',
            'minitems' => '0',
            'show_thumbs' => '1'
        )
    ),
    'ctvbeacons' => array(
        'displayCond' => 'FIELD:homepage:=:0',
        'exclude' => 1,
        'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_beacon.ctvbeacons',
        'config' => array(
            'type' => 'input',
            'size' => 5,
            'eval' => 'trim'
        ),
    ),
    'homeimage' => array(
        'exclude' => 1,
        'displayCond' => 'FIELD:homepage:=:1',
        'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.home-top-image',
        'config' => Array(
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'tx_pstables_domain_model_image',
            'size' => '1',
            'maxitems' => '1',
            'minitems' => '0',
            'show_thumbs' => '1'
        )
    ),
    'teasers' => array(
        'displayCond' => 'FIELD:homepage:=:1',
        'exclude' => 1,
        'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:tx_pstables_domain_model_teaser',
        'config' => Array(
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'tx_pstables_domain_model_teaser',
            'size' => '10',
            'maxitems' => '99',
            'minitems' => '0',
            'show_thumbs' => '1'
        )
    ),
    'homepage' => array(
        'label' => 'LLL:EXT:ps_tables/Resources/Private/Language/locallang_db.xlf:page.is-home-page',
        'exclude' => 1,
        'config' => array(
            'type' => 'check',
            'default' => '0'
        )
    ),
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages',
    'ctvtype, ctvcontent, ctvcontent2, ctvimage, homeimage, teasers, homepage', '', 'after:title');

$GLOBALS['TCA']['pages']['ctrl']['requestUpdate'] = ($GLOBALS['TCA']['pages']['ctrl']['requestUpdate'] ? $GLOBALS['TCA']['pages']['ctrl']['requestUpdate'] . ',' : '') . 'homepage';
