<?php
namespace Peaksourcing\PsUser\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * UserController
 */
class UserController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * userRepository
	 *
	 * @var \Peaksourcing\PsUser\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;

	/**
	 * action login
	 *
	 * @return void
	 * type=678
	 */
	public function loginAction() {
		
		$post = $this->getPost('booking_code');
		$apiToken = $this->generateRandomString();	
		$found = true;		
		
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'tx_psfeuser_rayseven_password=\'' . $post . '\'' 
		);
		//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
		if($row) {    
			$tokens = json_decode($row['tx_psfeuser_api_token']);
			$tokens[] = $apiToken;
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery( 
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_api_token' => json_encode($tokens),
				)
			);
		} else {
			$curl = curl_init();
			curl_setopt_array($curl, array( 
				CURLOPT_RETURNTRANSFER => 1,
				//CURLOPT_URL => 'https://rayseven.com/r7/runtime/also/ctv2016/api.php?code=enllyaiu729c',
				CURLOPT_URL => 'https://rayseven.com/r7/runtime/also/ctv2016/api.php?code=' . $post,
				CURLOPT_USERAGENT => 'Codular Sample cURL Request'
			));		
			$decoded = json_decode(curl_exec($curl));	
			
			if($decoded->code == '1'){	
				$result = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
					'fe_users',
					array(
						'pid' => 4,
						'tx_psfeuser_api_token' => json_encode(array($apiToken)),
						'tx_psfeuser_ticket_file_name' => $decoded->ticket,
						'tx_psfeuser_rayseven_password' => $post,
						'first_name' => $decoded->firstname,
						'last_name' => $decoded->surname,
						'tx_psfeuser_beacons' => '[]',
						'tx_psfeuser_favorites' => '[]',
						'tx_psfeuser_token' => '',
					)
				);
				$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
					'*',
					'fe_users',
					'uid=' . $GLOBALS['TYPO3_DB']->sql_insert_id()
				);
				$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
			}
			if($decoded->code == '2'){			
				$found = false;
			} 
		}
		$user = $this->userArray($row);
		
		if(!$found){
			$response['status'] = 400;
			$response['message'] = 'Login failed';
		} else {			
			$response['status'] = 200;
			$response['message'] = 'Successfully logged in';
			$response['data']['api_token'] = $apiToken;
			$response['data']['user'] = $user;
		}
				
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	}
	
	/**
	 * action logout
	 *
	 * @return void
	 * type=679	 
	 */
	public function logoutAction() { 
		$apiToken = $this->getApiToken();
		
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'tx_psfeuser_api_token LIKE \'%' . $apiToken . '%\''
		);
		
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
		if($row){
			$newTokens = array();
			foreach(json_decode($row['tx_psfeuser_api_token']) as $value){
				if($value != $apiToken){
					$newTokens[] = $value;
				}
			}
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_api_token' => json_encode($newTokens), 
				)
			);
			if ($GLOBALS['TYPO3_DB']->sql_affected_rows()) {      		
				$response['status'] = 200;
				$response['message'] = 'Successfully logged out';
			} else {
				$response['status'] = 400;
				$response['message'] = 'Logout failed';
			}
		} else {
			$response['status'] = 400;
			$response['message'] = 'Logout failed';
		}
		
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));		
	}
	
	/**
	 * action profile
	 *
	 * @return void
	 * type=680
	 */
	public function profileAction() {
		$apiToken = $this->getApiToken();
		$row = $this->getUserByApiToken($apiToken);	
		if ($row) {     
			$user = $this->userArray($row);			
			$response['status'] = 200;
			$response['message'] = 'Profile info';
			$response['data'] = $user;
		} else {
			$response['status'] = 400;
			$response['message'] = 'Profile failed';
		}
		
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	}

	/**
	 * action raffle
	 *
	 * @return void
	 * type=681
	 */
	
	public function raffleAction() {
		$param = 'beacons';
		$post = $this->getPost($param);
		$this->setValues($param, json_encode($post));
	}
	
	/**
	 * action push
	 *
	 * @return void
	 * type=682
	 */
	public function pushAction() {
		$param = 'token';
		$post = $this->getPost($param);
		$this->setValues($param, $post);
	}
	
	/**
	 * action favorites
	 *
	 * @return void
	 * type=683
	 */
	public function favoritesAction() {		
		$param = 'favorites';
		$post = $this->getPost($param);
		$this->setValues($param, json_encode($post));
	}
	
		
	
	/* library functions */
	
	public function setValues($param, $post){
		if(!$post) $post = '';
		$apiToken = $this->getApiToken();				
		$row = $this->getUserByApiToken($apiToken);	
		if ($row && ($post || $post == '')) {   
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_' . $param => $post, 
				)
			);
			
			$row = $this->getUserByApiToken($apiToken);			
			$user = $this->userArray($row);
			
			$response['status'] = 200;
			$response['message'] = 'Profile info';
			$response['data'] = $user;
		} else {
			$response['status'] = 400;
			$response['message'] = $param . ' update failed';
		}
		
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	}
	
	public function getUserByApiToken($apiToken){
		//$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'tx_psfeuser_api_token LIKE \'%' . $apiToken . '%\''
		);
		//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
		
		return $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
	}
	
	public function userArray($row){
		$user = array(
			'user_id' => $row['uid'],
			'name' => $row['first_name'] . ' ' . $row['last_name'],
			'booking_code' => $row['tx_psfeuser_rayseven_password'],
			'ticket_file_name' => $row['tx_psfeuser_ticket_file_name'],
			'beacons' => json_decode($row['tx_psfeuser_beacons']),
			'favorites' => json_decode($row['tx_psfeuser_favorites']),
			'token' => $row['tx_psfeuser_token'],
		);
		
		return $user;
	}
	
	public function getApiToken(){
		$headers = getallheaders();
			
		if(!$headers['User-Token']){
			$apiToken = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('api_token');
		} else {
			$apiToken = $headers['User-Token'];
		}
		
		return $apiToken;
	}
	
	public function getPost($param){
		$jsonPost = json_decode(file_get_contents('php://input'), true);		
		if(!$jsonPost[$param]){
			$post = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP($param);
		} else {
			$post = $jsonPost[$param];
		}
		
		return $post;
	}
	
	public function generateRandomString($length = 20) {
		$characters = '0123456789';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}	
}