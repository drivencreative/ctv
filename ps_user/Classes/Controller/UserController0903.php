<?php
namespace Peaksourcing\PsUser\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Miladin Bojic <miladin.bojic@gmail.com>, Peak Sourcing
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * UserController
 */
class UserController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	 /**
     * @return void
     */
    public function initializeAction() {		
		$this->writeToLog();
	}

	/**
	 * userRepository
	 *
	 * @var \Peaksourcing\PsUser\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;   

	/**
	 * action login
	 *
	 * @return void
	 * type=678
	 */
	public function loginAction() {
		$post = $this->getPost('booking_code'); 
		$apiToken = $this->generateRandomString();	
		$found = true;		
		
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'tx_psfeuser_rayseven_password=\'' . $post . '\'' 
		);
		//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
		$newObj = new \stdClass;
		$newObj->api = $apiToken;
		$newObj->device = ($this->getDeviceScreenInfo() ? $this->getDeviceScreenInfo() : "");
		$newObj->push = "";
		if($row) {    
			$tokens = json_decode($row['tx_psfeuser_api_token']);
			$tokens[] = $newObj;			
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery( 
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_api_token' => json_encode($tokens),
				)
			);
		} else {
			$curl = curl_init();
			curl_setopt_array($curl, array( 
				CURLOPT_RETURNTRANSFER => 1,
				//CURLOPT_URL => 'https://rayseven.com/r7/runtime/also/ctv2016/api.php?code=enllyaiu729c',
				CURLOPT_URL => 'https://rayseven.com/r7/runtime/also/ctv2016/api.php?code=' . $post,
				CURLOPT_USERAGENT => 'Codular Sample cURL Request'
			));		
			$decoded = json_decode(curl_exec($curl));	
			if($decoded->code == '1'){	
				//$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;
				$result = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
					'fe_users',
					array(
						'pid' => 4,
						'tx_psfeuser_api_token' => json_encode(array($newObj)),
						'tx_psfeuser_ticket_file_name' => $decoded->ticket,
						'tx_psfeuser_rayseven_password' => $post,
						'first_name' => $decoded->firstname,
						'last_name' => $decoded->surname,
						'tx_psfeuser_beacons' => '[]',
						'tx_psfeuser_favorites' => '[]',
						'tx_psfeuser_gewinnspiel_submitted' => '0',
						'title' => ($decoded->gender ? $decoded->gender : ''),
					)
				);
				//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
				$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
					'*',
					'fe_users',
					'uid=' . $GLOBALS['TYPO3_DB']->sql_insert_id()
				);
				$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
			}
			if($decoded->code == '2'){			
				$found = false;
			} 
		}
		
		if(!$found){
			$myfile = fopen('logs/loginfailure.txt', 'a') or die('Unable to open file!');
			$txt = 'booking_code=' . $post   . ' | action=login | ' . date('d:m:Y H:i:s');   
			fwrite($myfile, $txt . PHP_EOL);
			fclose($myfile);
			
			$response['status'] = 400;
			$response['message'] = 'Login failed';
		} else {			
			$user = $this->userArray($row);			
			
			$myfile = fopen('logs/loginlogout.txt', 'a') or die('Unable to open file!');
			$txt = 'uid=' . $row['uid'] . ' | token=' . $apiToken   . ' | action=login | device=' . $newObj->device . ' | ' . date('d:m:Y H:i:s') . ' | ' . $row['first_name'] . ' ' . $row['last_name'] ;  
			fwrite($myfile, $txt . PHP_EOL);
			fclose($myfile);
			
			$response['status'] = 200;
			$response['message'] = 'Successfully logged in';
			$response['data']['api_token'] = $apiToken;
			$response['data']['user'] = $user;
		}
				
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	}
	
	/**
	 * action logout
	 *
	 * @return void
	 * type=679	 
	 */
	public function logoutAction() { 
		$apiToken = $this->getApiToken();
		
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'tx_psfeuser_api_token LIKE \'%' . $apiToken . '%\''
		);
		
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
		if($row){
			$newTokens = array();
			$tokens = json_decode($row['tx_psfeuser_api_token']);
			foreach($tokens as $key => $obj){
				if($obj->api != $apiToken){
					$newTokens[] = $obj;
				} else {
					$device = $tokens[$key]->device;
				}
			}
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_api_token' => json_encode($newTokens), 
				)
			);
			
			$myfile = fopen('logs/loginlogout.txt', 'a') or die('Unable to open file!');
			$txt = 'uid=' . $row['uid'] . ' | token=' . $apiToken   . ' | action=logout | device=' . $device . ' | ' . date('d:m:Y H:i:s') . ' | ' . $row['first_name'] . ' ' . $row['last_name'] ;  
			fwrite($myfile, $txt . PHP_EOL);
			fclose($myfile);
			
			
			$response['status'] = 200;
			$response['message'] = 'Successfully logged out';
		} else {			
			$myfile = fopen('logs/try.txt', 'a') or die('Unable to open file!');
			$txt = 'token=' . $apiToken   . ' | action=logout | ' . date('d:m:Y H:i:s');  
			fwrite($myfile, $txt . PHP_EOL);
			fclose($myfile);
		
			$response['status'] = 400;
			$response['message'] = 'Logout failed';
		}
		
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));		
	}
	
	/**
	 * action profile
	 *
	 * @return void
	 * type=680
	 */
	public function profileAction() {
		$apiToken = $this->getApiToken();
		$row = $this->getUserByApiToken($apiToken);	
		if ($row) {     
			$user = $this->userArray($row);			
			$response['status'] = 200;
			$response['message'] = 'Profile info';
			$response['data'] = $user;
		} else {
			$response['status'] = 400;
			$response['message'] = 'Profile failed';
		}
		
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	}

	/**
	 * action raffle
	 *
	 * @return void
	 * type=681
	 */	
	public function raffleAction() {
		$apiToken = $this->getApiToken();				
		$row = $this->getUserByApiToken($apiToken);	
		if ($row) {   
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_gewinnspiel_submitted' => '1'
				)
			);
		}		

		$param = 'beacons';
		$post = $this->getPost($param);		
		$postCleaned = $this->cleanBeacons($post);		
		$this->setValues($param, json_encode($postCleaned));
	}
	
	/**
	 * action push
	 *
	 * @return void
	 * type=682
	 */
	public function pushAction() {
		$param = 'token';
		$post = $this->getPost($param);
		$apiToken = $this->getApiToken();
		
		//first remove apitoken if there allready a push same as $post
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'tx_psfeuser_api_token LIKE \'%' . $post . '%\''
		);		
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
		if($row){
			$tokens = json_decode($row['tx_psfeuser_api_token']);  
			foreach($tokens as $key => $obj){
				if($obj->push == $post && $obj->api != $apiToken){
					$tokens[$key]->push = '';
				} 
			}
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_api_token' => json_encode($tokens), 
				)
			);
		}		
		
		//insert push
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'tx_psfeuser_api_token LIKE \'%' . $apiToken . '%\''
		);		
		
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
		if($row){
			$newTokens = array();
			$newTokens = json_decode($row['tx_psfeuser_api_token']);  
			foreach($newTokens as $key => $obj){
				if($obj->api == $apiToken){
					$newTokens[$key]->push = $post;
				}
			}
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_api_token' => json_encode($newTokens), 
				)
			);
			
			$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
				'*',
				'fe_users',
				'uid =' . $row['uid']
			);		
			$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
			
			$user = $this->userArray($row);
			$response['status'] = 200;
			$response['message'] = 'Profile info';
			$response['data'] = $user;
		} else {
			$response['status'] = 400;
			$response['message'] = $param . ' update failed';
		}
		
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));		
	}
	
	/**
	 * action favorites
	 *
	 * @return void
	 * type=683
	 */
	public function favoritesAction() {		
		$param = 'favorites';
		$post = $this->getPost($param);
		$this->setValues($param, json_encode($post));
	}
	
	/**
	 * action savebeacons
	 *
	 * @return void
	 * type=684
	 */	
	public function savebeaconsAction() {
		$param = 'beacons';
		$post = $this->getPost('save_beacons');
		$postCleaned = $this->cleanBeacons($post);		
		$this->setValues($param, json_encode($postCleaned));
		/*
		$param = 'save_beacons';
		$post = $this->getPost($param);
		$beacons = array();
		foreach($post as $item){
			$currObj = new \stdClass;
			$currObj->uuid = '';
			$currObj->major = $item;
			$currObj->minor = '';
			$beacons[] = $currObj;
		}		
		$this->setValues($param, json_encode($beacons));
		*/
	}
		
	
	
	/* library functions */
	
	public function cleanBeacons($post){
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'uid',
			'tx_pstables_domain_model_beacon',
			'not hidden and not deleted' 
		);
		$uids = array();
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)){
			$uids[] = $row['uid'];
		}
		
		$postCleaned = array();
		if(is_array($post)){
			foreach($post as  $el){
				if(in_array($el, $uids)){
					$postCleaned[] = $el;
				}
			}
		}	

		return $postCleaned;
	}
	
	public function setValues($param, $post){
		if(!$post) $post = '';
		$apiToken = $this->getApiToken();				
		$row = $this->getUserByApiToken($apiToken);	
		if ($row && ($post || $post == '')) {   
			$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
				'fe_users',
				'uid=' . $row['uid'],
				array( 
					'tx_psfeuser_' . $param => $post
				)
			);
			
			$row = $this->getUserByApiToken($apiToken);			
			$user = $this->userArray($row);
			
			$response['status'] = 200;
			$response['message'] = 'Profile info';
			$response['data'] = $user;
		} else {
			$response['status'] = 400;
			$response['message'] = $param . ' update failed';
		}
		
		$this->view->assign('response', json_encode($response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	}
	
	public function getUserByApiToken($apiToken){
		//$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'fe_users',
			'tx_psfeuser_api_token LIKE \'%' . $apiToken . '%\''
		);
		//echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;
		
		return $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);
	}
	
	public function userArray($row){
		$apiToken = $this->getApiToken();			
		$tokens = json_decode($row['tx_psfeuser_api_token']);
		$token = '';
		foreach($tokens as $obj){
			if($obj->api == $apiToken){
				$token = $obj->push;
			}
		}
		/*
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'tx_pstables_domain_model_beacon',
			'uid in (' . implode(',', json_decode($row['tx_psfeuser_save_beacons'])) . ')'
		);
		$beacons = array();
		while($rowBeacons = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)){
			$currObj = new \stdClass;
			$currObj->major = $rowBeacons['uid'];
			$currObj->title = $rowBeacons['title'];
			$currObj->content = $rowBeacons['content'];
			$beacons[] = $currObj;
		}
		*/
		$beacons = array();
		foreach(json_decode($row['tx_psfeuser_beacons']) as $item){
			$currObj = new \stdClass;
			$currObj->UUID = '';
			$currObj->major = $item;
			$currObj->minor = '';
			$beacons[] = $currObj;
		}
		
		$user = array(
			'user_id' => $row['uid'],
			'gender' => $row['title'],
			'name' => $row['first_name'] . ' ' . $row['last_name'],
			'booking_code' => $row['tx_psfeuser_rayseven_password'],
			'ticket_file_name' => $row['tx_psfeuser_ticket_file_name'],
			'beacons' => $beacons,
			//'beacons' => json_decode($row['tx_psfeuser_beacons']),
			'favorites' => json_decode($row['tx_psfeuser_favorites']),
			'token' => $token,
			'gewinnspiel_submitted' => ($row['tx_psfeuser_gewinnspiel_submitted'] ? 'true' : 'false'),
		);
		
		$this->writeToLog(json_encode($user['beacons']));
		
		return $user;
	}
	
	public function getApiToken(){
		$headers = getallheaders();
			
		if(!$headers['User-Token']){
			$apiToken = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('api_token');
		} else {
			$apiToken = $headers['User-Token'];
		}
		
		return $apiToken;
	}
	
	public function getDeviceScreenInfo(){
		$headers = getallheaders();
			
		$deviceScreenInfo = $headers['DeviceScreenInfo'];
		
		return $deviceScreenInfo;
	}
	
	public function getPost($param){
		$jsonPost = json_decode(file_get_contents('php://input'), true);		
		if(!$jsonPost[$param]){
			$post = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP($param);
		} else {
			$post = $jsonPost[$param];
		}
		if(!$post) $post = '';
		return $post;
	}
	
	public function generateRandomString($length = 20) {
		$characters = '0123456789';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}	

	public function writeToLog($response = ''){
		$log = 'action=' . \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('type');		
		$log .= ' | headers: ';
		$certainHeaders = array('User-Token', 'DeviceScreenInfo');
		foreach(getallheaders() as $key => $item){
			if(in_array($key, $certainHeaders)){
				$log .= $key . '=' . $item . '   ';
			}
		}				
		$log .= ' | time: ' . date('d:m:Y H:i:s');
		$log .= ' | post: ' . file_get_contents('php://input');
		if($response){
			$log .= ' | response: ' . $response;
			$file = 'response';
		} else {
			$file = 'request';
		}
		
		$myfile = fopen('logs/' . $file . '.txt', 'a') or die('Unable to open file!');
		fwrite($myfile, $log . PHP_EOL . PHP_EOL);
		fclose($myfile);
	}
	
}