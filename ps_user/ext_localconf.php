<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Peaksourcing.' . $_EXTKEY,
	'User',
	array(
		'User' => 'login, logout, profile, raffle, push, favorites',
		
	),
	// non-cacheable actions
	array(
		'User' => '',
		
	)
);
